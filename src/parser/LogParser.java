package parser;

import java.io.*;
import java.util.*;
import java.util.regex.*;


public class LogParser {
	
	private static final String logDir = "logs/";
	
	public static void main(String args[]) throws IOException{
		File logFile = new File(LogParser.logDir);
		File[] logs = logFile.listFiles();
		
		if(args.length == 0){
			LogParser.getFetchCDF(logs, "fetchCDF.csv");
			LogParser.getBuildTime(logs, "buildCDF.csv");
			LogParser.getStabalizeBuildTime(logs, "stabalizeCDF.csv");
			LogParser.getTimeToValid(logs, "ttValidCDF.csv");
			LogParser.circuitTime(logs, "circuitTime.csv");
		}
		
		if(args[0].equals("f")){
			LogParser.getFetchCDF(logs, args[1]);
		}
		else if(args[0].equals("b")){
			LogParser.getBuildTime(logs, args[1]);
		}
		else if(args[0].equals("s")){
			LogParser.getStabalizeBuildTime(logs, args[1]);
		}
		else if(args[0].equals("v")){
			LogParser.getTimeToValid(logs, args[1]);
		}
		else{
			System.err.println("wtf...");
		}
	}
	
	public static void getFetchCDF(File[] logs, String outName) throws IOException{
		List<Integer> fetchTimes = new LinkedList<Integer>();
		Pattern fetchPatter = Pattern.compile("FT fetch took: ([0-9]++)");
		
		
		for(File tFile: logs){
			BufferedReader fRead = new BufferedReader(new FileReader(tFile));
			while(fRead.ready()){
				String poll = fRead.readLine();
				Matcher tMatcher = fetchPatter.matcher(poll);
				if(tMatcher.find()){
					fetchTimes.add(Integer.parseInt(tMatcher.group(1)));
				}
			}
			fRead.close();
		}
		
		Collections.sort(fetchTimes);
		
		BufferedWriter outBuff = new BufferedWriter(new FileWriter(outName));
		for(int counter = 0; counter < fetchTimes.size(); counter++){
			double fraction = ((double)counter + 1.0)/(double)fetchTimes.size();
			outBuff.write(fraction + "," + fetchTimes.get(counter) + "\n");
		}
		outBuff.close();
	}
	
	public static void getBuildTime(File[] logs, String outName) throws IOException{
		List<Integer> fetchTimes = new LinkedList<Integer>();
		Pattern fetchPatter = Pattern.compile("building first FT took: ([0-9]++)");
		
		
		for(File tFile: logs){
			BufferedReader fRead = new BufferedReader(new FileReader(tFile));
			while(fRead.ready()){
				String poll = fRead.readLine();
				Matcher tMatcher = fetchPatter.matcher(poll);
				if(tMatcher.find()){
					fetchTimes.add(Integer.parseInt(tMatcher.group(1)));
					break;
				}
			}
			fRead.close();
		}
		
		Collections.sort(fetchTimes);
		
		BufferedWriter outBuff = new BufferedWriter(new FileWriter(outName));
		for(int counter = 0; counter < fetchTimes.size(); counter++){
			double fraction = ((double)counter + 1.0)/(double)fetchTimes.size();
			outBuff.write(fraction + "," + fetchTimes.get(counter) + "\n");
		}
		outBuff.close();
	}
	
	public static void getStabalizeBuildTime(File[] logs, String outName) throws IOException{
		List<Integer> fetchTimes = new LinkedList<Integer>();
		Pattern otherPattern = Pattern.compile("building another node's table to sign took: ([0-9]++)");
		Pattern myPattern = Pattern.compile("stabalize took: ([0-9]++)");
		
		for(File tFile: logs){
			BufferedReader fRead = new BufferedReader(new FileReader(tFile));
			while(fRead.ready()){
				String poll = fRead.readLine();
				Matcher tMatcher = otherPattern.matcher(poll);
				if(tMatcher.find()){
					fetchTimes.add(Integer.parseInt(tMatcher.group(1)));
				}
				tMatcher = myPattern.matcher(poll);
				if(tMatcher.find()){
					fetchTimes.add(Integer.parseInt(tMatcher.group(1)));
				}
			}
			fRead.close();
		}
		
		Collections.sort(fetchTimes);
		
		BufferedWriter outBuff = new BufferedWriter(new FileWriter(outName));
		for(int counter = 0; counter < fetchTimes.size(); counter++){
			double fraction = ((double)counter + 1.0)/(double)fetchTimes.size();
			outBuff.write(fraction + "," + fetchTimes.get(counter) + "\n");
		}
		outBuff.close();
	}
	
	public static void getTimeToValid(File[] logs, String outName) throws IOException{
		List<Long> fetchTimes = new LinkedList<Long>();
		Pattern startPattern = Pattern.compile("stabalizing: [0-9a-e]++ started at: ([0-9]++)");
		Pattern donePattern = Pattern.compile("stabalize now valid [0-9a-e]++ at ([0-9]++)");
		
		
		for(File tFile: logs){
			BufferedReader fRead = new BufferedReader(new FileReader(tFile));
			long currentStart = -1;
			while(fRead.ready()){
				String poll = fRead.readLine();
				Matcher tMatcher = startPattern.matcher(poll);
				if(tMatcher.find()){
					currentStart = Long.parseLong(tMatcher.group(1));
				}
				tMatcher = donePattern.matcher(poll);
				if(tMatcher.find()){
					if(currentStart == -1){
						continue;
					}
					
					fetchTimes.add((Long.parseLong(tMatcher.group(1)) - currentStart));
					currentStart = -1;
				}
			}
			fRead.close();
		}
		
		Collections.sort(fetchTimes);
		
		BufferedWriter outBuff = new BufferedWriter(new FileWriter(outName));
		for(int counter = 0; counter < fetchTimes.size(); counter++){
			double fraction = ((double)counter + 1.0)/(double)fetchTimes.size();
			outBuff.write(fraction + "," + fetchTimes.get(counter) + "\n");
		}
		outBuff.close();
	}
	
	public static void circuitTime(File[] logs, String outName) throws IOException{
		List<Integer> fetchTimes = new LinkedList<Integer>();
		Pattern fetchPatter = Pattern.compile("FT fetch took: ([0-9]++)");
		
		
		for(File tFile: logs){
			BufferedReader fRead = new BufferedReader(new FileReader(tFile));
			while(fRead.ready()){
				String poll = fRead.readLine();
				Matcher tMatcher = fetchPatter.matcher(poll);
				if(tMatcher.find()){
					fetchTimes.add(Integer.parseInt(tMatcher.group(1)));
				}
			}
			fRead.close();
		}
		
		List<Integer> circuitTimes = new LinkedList<Integer>();
		Random rng = new Random();
		for(int counter = 0; counter < 5000; counter++){
			int sum = 0;
			for(int hopCount = 0; hopCount < 6; hopCount++){
				int roll = rng.nextInt(fetchTimes.size());
				sum += fetchTimes.get(roll);
			}
			circuitTimes.add(sum);
		}
		
		
		Collections.sort(circuitTimes);
		BufferedWriter outBuff = new BufferedWriter(new FileWriter(outName));
		for(int counter = 0; counter < circuitTimes.size(); counter++){
			double fraction = ((double)counter + 1.0)/(double)circuitTimes.size();
			outBuff.write(fraction + "," + circuitTimes.get(counter) + "\n");
		}
		outBuff.close();
	}

}
