package testing;
import rope.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

import rope.pub.core.RopeNode;
import rope.pub.data.RopeContact;
import rope.pub.data.RopeID;
import rope.*;
public class NetTest {


	public static void main(String[] args) throws UnknownHostException, InterruptedException {
		byte[] a = new byte[1];
		byte[] b = new byte[1];
		a[0] = 0;
		b[0] = (byte)128;
		RopeContact wellKnown = new RopeContact(new RopeID(a), InetAddress.getByName("127.0.0.1"), 14000);
		RopeContact otherWellKnown = new RopeContact(new RopeID(b), InetAddress.getByName("127.0.0.1"), 14001);
		
		RopeNode wkNode = new RopeNode(wellKnown);
		RopeNode owkNode = new RopeNode(otherWellKnown);
		wkNode.hack(otherWellKnown);
		owkNode.hack(wellKnown);
		
		int testCount = 1;
		RopeNode testNodes[] = new RopeNode[testCount];
		for(int counter = 0; counter < testCount; counter++){
			RopeContact temp = new RopeContact(new RopeID(), InetAddress.getByName("127.0.0.1"), 15000+ counter);
			testNodes[counter] = new RopeNode(temp);
			testNodes[counter].join();
			Thread.sleep(500);
		}
		
		Thread.sleep(12000);
		//testNodes[1].kill();
		
		Thread.sleep(20000);
		wkNode.leave();
		owkNode.leave();
		for(int counter = 0; counter < testCount; counter++){
			testNodes[counter].leave();
		}
	}

}
