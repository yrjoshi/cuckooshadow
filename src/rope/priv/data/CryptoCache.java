package rope.priv.data;

import shadowcrypto.*;
import rope.pub.data.*;

import java.util.*;

public class CryptoCache {

	private RopeContact myContact = null;

	private RSAKey myKey = null;
	
	private HashMap<RopeContact, RSAPK> otherKeys = null;
	
	public CryptoCache(RopeContact me){
		this.myContact = me;
		this.myKey = RSASignature.generateKey();
		this.otherKeys = new HashMap<RopeContact, RSAPK>();
	}
	
	public RSAPK getMyPubKey(){
		return this.myKey.getPk();
	}
	
	public synchronized boolean hasKey(RopeContact otherNode){
		return this.otherKeys.containsKey(otherNode);
	}
	
	public synchronized void cacheKey(RopeContact otherNode, RSAPK key){
		this.otherKeys.put(otherNode, key);
	}
	
	public synchronized boolean sigValid(RopeContact signer, byte[] data, byte[] sig){
		if(signer.getTheID().equals(this.myContact.getTheID())){
			return RSASignature.verify(data, sig, this.myKey.getPk());
		}
		
		return RSASignature.verify(data, sig, this.otherKeys.get(signer));
	}
	
	public byte[] sign(byte[] data){
		return RSASignature.sign(data, this.myKey);
	}
}
