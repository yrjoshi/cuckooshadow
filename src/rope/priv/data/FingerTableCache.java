package rope.priv.data;

import rope.priv.core.MaxLogger;
import rope.pub.data.*;

import java.util.*;
import java.util.concurrent.Semaphore;

public class FingerTableCache implements Runnable {

	private RopeContact myContact = null;

	private FingerTable myFingerTable = null;

	private HashMap<RopeContact, Long> timeStampMap = null;

	private HashMap<RopeContact, FingerTable> tableMap = null;

	private HashMap<RopeContact, Long> lastCachedMap = null;

	private HashMap<RopeContact, Semaphore> actionMap = null;
	
	private MaxLogger logs = null;

	public static final long VALID_WINDOW = 150000;

	public static final long GRACE_WINDOW = 1000;

	public static final long CLEAN_INTERVAL = 300000;

	public FingerTableCache(RopeContact myContact, MaxLogger log) {
		this.myContact = myContact;
		this.myFingerTable = null;
		this.timeStampMap = new HashMap<RopeContact, Long>();
		this.tableMap = new HashMap<RopeContact, FingerTable>();
		this.lastCachedMap = new HashMap<RopeContact, Long>();
		this.actionMap = new HashMap<RopeContact, Semaphore>();
		this.logs = log;
	}

	public synchronized void cacheFT(FingerTable ft) {

		/*
		 * Don't cache our own finger table
		 */
		if (ft.getTableOwner().equals(this.myContact)) {
			return;
		}

		//XXX REMOVE ME
//		if (System.currentTimeMillis() - ft.getLastUpdated() > FingerTableCache.VALID_WINDOW) {
//			System.err.println("asked to add expired FT!");
//			return;
//		}

		if (this.tableMap.containsKey(ft.getTableOwner())
				&& this.tableMap.get(ft.getTableOwner()).getLastUpdated() > ft.getLastUpdated()) {
			this.logs.write("asked to cache an old copy of FT");
			return;
		}

		this.timeStampMap.put(ft.getTableOwner(), System.currentTimeMillis());
		this.tableMap.put(ft.getTableOwner(), ft);
		this.lastCachedMap.put(ft.getTableOwner(), System.currentTimeMillis());
		this.actionMap.get(ft.getTableOwner()).release();
		this.logs.write("caching new copy of: " + ft.getTableOwner() + " at " + this.myContact);
	}

	public synchronized FingerTable getCachedFT(RopeContact otherHost) {
		if (otherHost.equals(this.myContact)) {
			return this.myFingerTable;
		}

		if (!this.timeStampMap.containsKey(otherHost)) {
			return null;
		}

		if (System.currentTimeMillis() - this.timeStampMap.get(otherHost) > FingerTableCache.VALID_WINDOW) {
			this.timeStampMap.remove(otherHost);
			this.tableMap.remove(otherHost);
			this.logs.write("asked for expired ft for: " + otherHost + " at: " + this.myContact);
			return null;
		}

		return this.tableMap.get(otherHost);
	}

	public synchronized long getLastCache(RopeContact node) {
		if (node.equals(this.myContact)) {
			return this.myFingerTable.getLastUpdated();
		}

		long retValue = 0;
		if (this.tableMap.containsKey(node)) {
			retValue = this.tableMap.get(node).getLastUpdated();
			if (System.currentTimeMillis() - this.timeStampMap.get(node) > FingerTableCache.VALID_WINDOW) {
				retValue = 0;
			}
		}
		return retValue;
	}

	public boolean shouldAttemptFetch(RopeContact node) {

		if(node.equals(this.myContact)){
			return false;
		}
		
		Semaphore theSem = null;
		synchronized (this.actionMap) {
			if (!this.actionMap.containsKey(node)) {
				this.actionMap.put(node, new Semaphore(1));
			}
			theSem = this.actionMap.get(node);
		}

		try {
			theSem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		boolean timeOK = false;
		if (!this.lastCachedMap.containsKey(node)) {
			timeOK = true;
		} else {
			long lastFetch = this.lastCachedMap.get(node);
			lastFetch = System.currentTimeMillis() - lastFetch;
			if(lastFetch > FingerTableCache.GRACE_WINDOW){
				timeOK = true;
			}
		}
		
		if(!timeOK){
			theSem.release();
		}

		return timeOK;
	}
	
	public void reportLookupFailed(RopeContact node){
		this.lastCachedMap.put(node, System.currentTimeMillis());
		this.actionMap.get(node).release();
	}
	
	public void purgeTable(RopeContact node){
		this.timeStampMap.remove(node);
		this.tableMap.remove(node);
	}

	public void run() {

		HashSet<RopeContact> removeSet = new HashSet<RopeContact>();
		while (true) {

			//TODO what about interrupted exception?
			try {
				Thread.sleep(FingerTableCache.CLEAN_INTERVAL);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			synchronized (this) {
				for (RopeContact tContact : this.timeStampMap.keySet()) {
					if (System.currentTimeMillis() - this.timeStampMap.get(tContact) > FingerTableCache.VALID_WINDOW) {
						removeSet.add(tContact);
					}
				}

				for (RopeContact tContact : removeSet) {
					this.timeStampMap.remove(tContact);
					this.tableMap.remove(tContact);
				}
			}
			removeSet.clear();
		}
	}

	public String toString() {
		String retString = "";
		for (RopeContact tContact : this.tableMap.keySet()) {
			retString += tContact + " : " + this.timeStampMap.get(tContact) + "\n";
			retString += this.tableMap.get(tContact) + "\n";
		}

		return retString;
	}

	public void updateMyFT(FingerTable myFT) {
		this.myFingerTable = myFT;
	}
	
	public RopeContact getMe(){
		return this.myContact;
	}
}
