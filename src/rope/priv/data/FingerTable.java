package rope.priv.data;

import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import rope.io.ByteOps;
import rope.pub.data.*;

public class FingerTable {

	private RopeContact myContact = null;

	private RopeID[] fingerStarts = null;

	private RopeContact[] fingerNodes = null;

	private HashMap<RopeID, HashSet<RopeContact>> otherNodeShadows = null;

	private RopeContact[] prevNodes = null;

	private RopeContact[] succNodes = null;

	private HashMap<RopeContact, byte[]> sigs = null;

	private long lastUpdated = -1;

	public static final int SUCC_COUNT = 10;

	public static final int SIG_SIZE = 128;

	public static final int VALID_THRESH = 1;

	public FingerTable(RopeContact myContact, long updateTime) {
		this.myContact = myContact;
		this.populateFingerStarts();

		this.prevNodes = new RopeContact[FingerTable.SUCC_COUNT];
		this.succNodes = new RopeContact[FingerTable.SUCC_COUNT];
		this.otherNodeShadows = new HashMap<RopeID, HashSet<RopeContact>>();
		this.lastUpdated = updateTime;

		this.sigs = new HashMap<RopeContact, byte[]>();
	}

	public FingerTable(byte[] wireBytes) throws UnknownHostException {
		this.lastUpdated = ByteOps.wordToLong(ByteOps.subArray(wireBytes, 0, 8));
		this.myContact = RopeContact.parseBytesToContact(ByteOps.subArray(wireBytes, 8, RopeContact.BYTE_SIZE));
		this.populateFingerStarts();
		this.otherNodeShadows = new HashMap<RopeID, HashSet<RopeContact>>();
		this.sigs = new HashMap<RopeContact, byte[]>();

		/*
		 * Next comes the preds and then the succs
		 */
		byte[] predBytes = ByteOps.subArray(wireBytes, RopeContact.BYTE_SIZE + 8, RopeContact.BYTE_SIZE
				* FingerTable.SUCC_COUNT);
		byte[] succBytes = ByteOps.subArray(wireBytes, RopeContact.BYTE_SIZE + 8 + RopeContact.BYTE_SIZE
				* FingerTable.SUCC_COUNT, RopeContact.BYTE_SIZE * FingerTable.SUCC_COUNT);
		this.prevNodes = new RopeContact[FingerTable.SUCC_COUNT];
		this.succNodes = new RopeContact[FingerTable.SUCC_COUNT];
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			this.prevNodes[counter] = RopeContact.parseBytesToContact(ByteOps.subArray(predBytes, RopeContact.BYTE_SIZE
					* counter, RopeContact.BYTE_SIZE));
			this.succNodes[counter] = RopeContact.parseBytesToContact(ByteOps.subArray(succBytes, RopeContact.BYTE_SIZE
					* counter, RopeContact.BYTE_SIZE));
		}

		/*
		 * Get the number of unique fingers we're about to recieve, and then
		 * parse them
		 */
		int startOfFingerBytes = RopeContact.BYTE_SIZE * (FingerTable.SUCC_COUNT * 2 + 1) + 12;
		int fingerCount = ByteOps.wordToInt(ByteOps.subArray(wireBytes, startOfFingerBytes - 4, 4));
		byte[] fingerBytes = ByteOps.subArray(wireBytes, startOfFingerBytes, wireBytes.length - startOfFingerBytes);
		int currentPos = 0;
		for (int counter = 0; counter < fingerCount; counter++) {
			RopeContact tContact = RopeContact.parseBytesToContact(ByteOps.subArray(fingerBytes, currentPos,
					RopeContact.BYTE_SIZE));
			this.offerFinger(tContact);

			HashSet<RopeContact> tShadowSet = new HashSet<RopeContact>();
			int shadowCount = ByteOps.wordToInt(ByteOps.subArray(fingerBytes, currentPos + RopeContact.BYTE_SIZE, 4));
			for (int innerCounter = 0; innerCounter < shadowCount; innerCounter++) {
				RopeContact tShadow = RopeContact.parseBytesToContact(ByteOps.subArray(fingerBytes, currentPos
						+ RopeContact.BYTE_SIZE * (innerCounter + 1) + 4, RopeContact.BYTE_SIZE));
				tShadowSet.add(tShadow);
			}
			this.otherNodeShadows.put(tContact.getTheID(), tShadowSet);
			currentPos += RopeContact.BYTE_SIZE * (shadowCount + 1) + 4;
		}
	}

	private void populateFingerStarts() {
		this.fingerStarts = new RopeID[RopeID.ID_BYTE_LEN * 8];
		this.fingerNodes = new RopeContact[RopeID.ID_BYTE_LEN * 8];
		List<RopeID> tempFingerList = this.myContact.getTheID().getFingerStarts();
		for (int counter = 0; counter < tempFingerList.size(); counter++) {
			this.fingerStarts[counter] = tempFingerList.get(counter);
			this.fingerNodes[counter] = this.myContact;
		}
	}

	public RopeContact getTableOwner() {
		return this.myContact;
	}

	public synchronized RopeContact getSuccessor(RopeID targetID) {

		/*
		 * We assume that this is a lone node, therefore return ourself
		 */
		if (this.internalMyPred() == null) {
			return this.myContact;
		}

		/*
		 * Check to see if we are the successor
		 */
		if (targetID.betweenInclusiveForward(this.myContact.getTheID(), this.internalMyPred().getTheID())) {
			return this.myContact;
		}

		/*
		 * Check to see if our successor is the successor
		 */
		if (targetID.betweenInclusiveForward(this.internalMySucc().getTheID(), this.myContact.getTheID())) {
			return this.internalMySucc();
		}

		return this.internalGetPredacessor(targetID);
	}

	public synchronized RopeContact getPredacessor(RopeID targetID) {
		if (targetID.equals(this.myContact.getTheID())) {
			return this.prevNodes[0];
		}

		return this.internalGetPredacessor(targetID);
	}

	private RopeContact internalGetPredacessor(RopeID targetID) {
		for (int counter = this.fingerNodes.length - 1; counter >= 0; counter--) {
			if (this.fingerNodes[counter] == null) {
				continue;
			}

			if (this.fingerNodes[counter].getTheID().betweenExclusive(targetID, this.myContact.getTheID())) {
				return this.fingerNodes[counter];
			}
		}

		return this.myContact;
	}

	private RopeContact internalMyPred() {
		return this.prevNodes[0];
	}

	private RopeContact internalMySucc() {
		return this.succNodes[0];
	}

	public synchronized boolean willResultInChange(RopeContact newContact) {
		/*
		 * check to see if it is a better finger for each slot
		 */
		for (int counter = this.fingerNodes.length - 1; counter >= 0; counter--) {
			if (newContact.getTheID().betweenInclusiveBackward(this.fingerNodes[counter].getTheID(),
					this.fingerStarts[counter])) {
				return true;
			}
		}

		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			if (this.prevNodes[counter] == null) {
				return true;
			} else if (this.prevNodes[counter].getTheID().equals(newContact.getTheID())) {
				break;
			} else {
				if (newContact.getTheID().betweenExclusive(this.myContact.getTheID(),
						this.prevNodes[counter].getTheID())) {
					return true;
				}
			}
		}

		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			if (this.succNodes[counter] == null) {
				return true;
			} else if (this.succNodes[counter].getTheID().equals(newContact.getTheID())) {
				break;
			} else {
				if (newContact.getTheID().betweenExclusive(this.succNodes[counter].getTheID(),
						this.myContact.getTheID())) {
					return true;
				}
			}
		}

		return false;
	}

	public synchronized boolean offerFinger(RopeContact newContact) {
		boolean changedSomething = false;

		/*
		 * check to see if it is a better finger for each slot
		 */
		for (int counter = this.fingerNodes.length - 1; counter >= 0; counter--) {
			if (newContact.getTheID().betweenInclusiveBackward(this.fingerNodes[counter].getTheID(),
					this.fingerStarts[counter])) {
				this.fingerNodes[counter] = newContact;
				changedSomething = true;
			}
		}

		if (newContact.getTheID().equals(this.myContact.getTheID())) {
			return changedSomething;
		}

		/*
		 * update our previous node list
		 */
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {

			if (this.prevNodes[counter] == null) {
				this.prevNodes[counter] = newContact;
				changedSomething = true;
				break;
			} else if (this.prevNodes[counter].getTheID().equals(newContact.getTheID())) {
				break;
			} else {
				if (newContact.getTheID().betweenExclusive(this.myContact.getTheID(),
						this.prevNodes[counter].getTheID())) {
					for (int moveCounter = FingerTable.SUCC_COUNT - 2; moveCounter >= counter; moveCounter--) {
						this.prevNodes[moveCounter + 1] = this.prevNodes[moveCounter];
					}
					this.prevNodes[counter] = newContact;
					changedSomething = true;
					break;
				}
			}
		}

		/*
		 * update our successor list
		 */
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {

			if (this.succNodes[counter] == null) {
				this.succNodes[counter] = newContact;
				changedSomething = true;
				break;
			} else if (this.succNodes[counter].getTheID().equals(newContact.getTheID())) {
				break;
			} else {
				if (newContact.getTheID().betweenExclusive(this.succNodes[counter].getTheID(),
						this.myContact.getTheID())) {
					for (int moveCounter = FingerTable.SUCC_COUNT - 2; moveCounter >= counter; moveCounter--) {
						this.succNodes[moveCounter + 1] = this.succNodes[moveCounter];
					}
					this.succNodes[counter] = newContact;
					changedSomething = true;
					break;
				}
			}
		}

		return changedSomething;
	}

	public String toString() {
		String retString = "FingerTable [prevNode=" + Arrays.toString(this.prevNodes) + "]\n" + "[succNode="
				+ Arrays.toString(this.succNodes) + "]\n" + "[fingerNodes=" + Arrays.toString(fingerNodes) + "]\n";
		HashSet<RopeContact> shadowedNodes = this.internalGetShadowedFingers();
		for (RopeContact tContact : shadowedNodes) {
			retString += "   " + tContact + " : ";
			if(this.otherNodeShadows.get(tContact.getTheID()) == null){
				System.err.println("BZZZAAAHHH: " + tContact + " me: " + this.myContact);
				continue;
			}
			for (RopeContact tShadow : this.otherNodeShadows.get(tContact.getTheID())) {
				retString += tShadow + ",";
			}
			retString += "\n";
		}
		retString += "last updated: " + this.lastUpdated;
		retString += "\nsigs: ";
		for (RopeContact tContact : this.sigs.keySet()) {
			retString += tContact.toString() + ",";
		}
		return retString;
	}

	public synchronized RopeContact getMyPredSlot(int slot) {
		return this.prevNodes[slot];
	}

	public synchronized RopeContact getMySuccSlot(int pos) {
		return this.succNodes[pos];
	}

	public synchronized void successorDeath(int pos) {
		for (int counter = pos + 1; counter < FingerTable.SUCC_COUNT; counter++) {
			this.succNodes[counter - 1] = this.succNodes[counter];
		}
		this.succNodes[FingerTable.SUCC_COUNT - 1] = null;
	}

	public synchronized int pushNewSuccIn() {
		int stopPos = 0;
		for (int counter = 0; counter < this.fingerNodes.length; counter++) {
			if (this.fingerNodes[counter].getTheID().betweenExclusive(this.succNodes[0].getTheID(),
					this.myContact.getTheID())) {
				this.fingerNodes[counter] = this.succNodes[0];
				stopPos++;
			} else {
				break;
			}
		}
		return stopPos;
	}

	public synchronized void predDeath(int pos) {
		for (int counter = pos + 1; counter < FingerTable.SUCC_COUNT; counter++) {
			this.prevNodes[counter - 1] = this.prevNodes[counter];
		}
		this.prevNodes[FingerTable.SUCC_COUNT - 1] = null;
	}

	public synchronized void overrideFinger(int pos, RopeContact newFinger) {
		this.fingerNodes[pos] = newFinger;
	}

	public synchronized HashSet<RopeContact> getMyShadows() {
		HashSet<RopeContact> retSet = new HashSet<RopeContact>();
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			if (this.prevNodes[counter] != null) {
				retSet.add(this.prevNodes[counter]);
			}
			if (this.succNodes[counter] != null) {
				retSet.add(this.succNodes[counter]);
			}
		}

		return retSet;
	}

	public synchronized HashSet<RopeID> getShadowedFingers() {
		HashSet<RopeID> retSet = new HashSet<RopeID>();
		for (RopeContact tContact : this.internalGetShadowedFingers()) {
			retSet.add(tContact.getTheID());
		}
		return retSet;
	}

	private HashSet<RopeContact> internalGetShadowedFingers() {
		HashSet<RopeContact> retSet = new HashSet<RopeContact>();
		for (int counter = 0; counter < this.fingerNodes.length; counter++) {
			if (this.fingerNodes[counter] != null) {
				retSet.add(this.fingerNodes[counter]);
			}
		}
		retSet.add(this.internalMyPred());
		return retSet;
	}

	public synchronized void setFingerShadows(RopeID theFinger, RopeContact[] predShadows, RopeContact[] succShadows) {
		if (!this.otherNodeShadows.containsKey(theFinger)) {
			this.otherNodeShadows.put(theFinger, new HashSet<RopeContact>());
		}

		HashSet<RopeContact> tempSet = this.otherNodeShadows.get(theFinger);
		tempSet.clear();
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			if (predShadows[counter] != null) {
				tempSet.add(predShadows[counter]);
			}
			if (succShadows[counter] != null) {
				tempSet.add(succShadows[counter]);
			}
		}
	}
	
	public synchronized void resetFingerShadows(RopeID theFinger){
		if (!this.otherNodeShadows.containsKey(theFinger)) {
			this.otherNodeShadows.put(theFinger, new HashSet<RopeContact>());
		}

		HashSet<RopeContact> tempSet = this.otherNodeShadows.get(theFinger);
		tempSet.clear();		
	}
	
	public synchronized void setFingerShadows(RopeID theFinger, RopeContact[] shadows){
		if (!this.otherNodeShadows.containsKey(theFinger)) {
			this.otherNodeShadows.put(theFinger, new HashSet<RopeContact>());
		}

		HashSet<RopeContact> tempSet = this.otherNodeShadows.get(theFinger);
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			if (shadows[counter] != null) {
				tempSet.add(shadows[counter]);
			}
		}
	}

	//XXX this function might be a bit slow since it allocates byte arrays over and over and over, fix this?
	public synchronized byte[] dumpTableToBytes() {
		/*
		 * Add my contact
		 */
		byte[] retArray = ByteOps.catByteArrays(ByteOps.longToWord(this.lastUpdated), RopeContact
				.getBytes(this.myContact));

		/*
		 * Add my shadows
		 */
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			retArray = ByteOps.catByteArrays(retArray, RopeContact.getBytes(this.prevNodes[counter]));
		}
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			retArray = ByteOps.catByteArrays(retArray, RopeContact.getBytes(this.succNodes[counter]));
		}

		/*
		 * Add my fingers and their shadows, and my pred just to add his shadows
		 */
		HashSet<RopeContact> uniqueFingers = this.internalGetShadowedFingers();
		retArray = ByteOps.catByteArrays(retArray, ByteOps.intToWord(uniqueFingers.size()));
		HashSet<RopeContact> tempFingerShadows = null;
		HashSet<RopeContact> wroteShadows = new HashSet<RopeContact>();
		while (uniqueFingers.size() > 0) {
			RopeContact tContact = null;
			for (RopeContact iterContact : uniqueFingers) {
				if (tContact == null) {
					tContact = iterContact;
				} else if (tContact.getTheID().compareTo(iterContact.getTheID()) < 0) {
					tContact = iterContact;
				}
			}
			uniqueFingers.remove(tContact);

			tempFingerShadows = this.otherNodeShadows.get(tContact.getTheID());
			retArray = ByteOps.catByteArrays(retArray, RopeContact.getBytes(tContact));

			if (tempFingerShadows == null) {
				retArray = ByteOps.catByteArrays(retArray, ByteOps.intToWord(0));
			} else {
				retArray = ByteOps.catByteArrays(retArray, ByteOps.intToWord(tempFingerShadows.size()));
				wroteShadows.clear();

				while (wroteShadows.size() != tempFingerShadows.size()) {
					RopeContact tShadow = null;
					for (RopeContact iterShadow : tempFingerShadows) {
						if (wroteShadows.contains(iterShadow)) {
							continue;
						} else if (tShadow == null) {
							tShadow = iterShadow;
						} else if (tShadow.getTheID().compareTo(iterShadow.getTheID()) < 0) {
							tShadow = iterShadow;
						}
					}

					retArray = ByteOps.catByteArrays(retArray, RopeContact.getBytes(tShadow));
					wroteShadows.add(tShadow);
				}
			}
		}

		return retArray;
	}

	public byte[] getSigBytes() {
		byte[] retArray = ByteOps.intToWord(this.sigs.size());
		for (RopeContact tContact : this.sigs.keySet()) {
			retArray = ByteOps.catByteArrays(retArray, ByteOps.catByteArrays(RopeContact.getBytes(tContact), this.sigs
					.get(tContact)));
		}
		return retArray;
	}
	
	private synchronized byte[] getOnlyFingers(){
		byte[] retArray = RopeContact.getBytes(this.myContact);
		HashSet<RopeContact> uniqueFingers = this.internalGetShadowedFingers();
		retArray = ByteOps.catByteArrays(retArray, ByteOps.intToWord(uniqueFingers.size()));
		while (uniqueFingers.size() > 0) {
			RopeContact tContact = null;
			for (RopeContact iterContact : uniqueFingers) {
				if (tContact == null) {
					tContact = iterContact;
				} else if (tContact.getTheID().compareTo(iterContact.getTheID()) < 0) {
					tContact = iterContact;
				}
			}
			uniqueFingers.remove(tContact);
			retArray = ByteOps.catByteArrays(retArray, RopeContact.getBytes(tContact));
		}
		
		return retArray;
	}

	public byte[] getHash() {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			System.exit(-3);
		}

		byte[] hash = md.digest(this.getOnlyFingers());
		return hash;
	}

	public synchronized void markAsUpdated() {
		long newTime = System.currentTimeMillis();
		if (newTime != this.lastUpdated) {
			this.lastUpdated = System.currentTimeMillis();
		} else {
			this.lastUpdated++;
		}
		this.sigs.clear();
	}

	public synchronized long getLastUpdated() {
		return this.lastUpdated;
	}

	public synchronized void addSig(RopeContact contact, byte[] sig) {
		this.sigs.put(contact, sig);
	}

	public synchronized HashMap<RopeContact, byte[]> getSigs() {
	    HashMap<RopeContact, byte[]> retMap = new HashMap<RopeContact, byte[]>();
	    for(RopeContact tCon: this.sigs.keySet()){
		retMap.put(tCon, this.sigs.get(tCon));
	    }
	    return retMap;
	}

	public boolean equals(Object rhs) {
		FingerTable rhsTable = (FingerTable) rhs;

		if (!this.myContact.equals(rhsTable.myContact)) {
			System.out.println("MYCONTACT");
			return false;
		}

		for (int counter = 0; counter < this.fingerNodes.length; counter++) {
			if (!this.fingerNodes[counter].equals(rhsTable.fingerNodes[counter])) {
				System.out.println("FINGER");
				return false;
			}
		}

		for (int counter = 0; counter < this.succNodes.length; counter++) {
			if (this.succNodes[counter] == null || rhsTable.succNodes[counter] == null) {
				if (!(this.succNodes[counter] == null && rhsTable.succNodes[counter] == null)) {
					System.out.println("SUCC NULL");
					return false;
				}
			} else {
				if (!this.succNodes[counter].equals(rhsTable.succNodes[counter])) {
					System.out.println("SUCC");
					return false;
				}
			}

			if (this.prevNodes[counter] == null || rhsTable.prevNodes[counter] == null) {
				if (!(this.prevNodes[counter] == null && rhsTable.prevNodes[counter] == null)) {
					System.out.println("PRED NULL");
					return false;
				}
			} else {
				if (!this.prevNodes[counter].equals(rhsTable.prevNodes[counter])) {
					System.out.println("PRED");
					return false;
				}
			}
		}

		for (RopeID tShadowed : this.otherNodeShadows.keySet()) {
			HashSet<RopeContact> other = rhsTable.otherNodeShadows.get(tShadowed);
			if (other == null) {
				System.out.println("NO SHADOW: " + tShadowed);
				return false;
			}
			if (other.size() != this.otherNodeShadows.get(tShadowed).size()) {
				System.out.println("SHADOW SIZE");
				return false;
			}
			for (RopeContact tShadow : this.otherNodeShadows.get(tShadowed)) {
				if (!other.contains(tShadow)) {
					System.out.println("SHADOW CONTENTS");
					return false;
				}
			}
		}

		return true;
	}
	
	public synchronized int getFirstEmptyFinger(){
		for(int counter = 0; counter < this.fingerNodes.length; counter++){
			if(this.fingerNodes[counter].equals(this.myContact)){
				return counter;
			}
		}
		return -1;
	}
}
