package rope.priv.core;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.concurrent.Semaphore;

import rope.pub.data.*;
import rope.priv.data.*;

/**
 * Slightly mis-named class currently. this class wraps the code needed to
 * manage a lookup. You can think of this as the low level component of
 * RopeLocalRMI. RopeLocalRMI deals with "big" behavior: bootstraping or
 * stabalizing for example, while this class deals with "small pieces" of the
 * big task: completing a single lookup. This class will create many network
 * connections as it attempts to locate the correct result of a querry.
 * 
 */
/*
 * TODO this code could be multi-threaded, in fact in the future I'm fairly sure
 * we're going to want to multi-thread this, as of now this is not ran in it's
 * own thread.
 */
public class RopeRMIRequest implements Runnable {

	/**
	 * The node we are suppose to start the lookup from.
	 */
	private RopeContact startingPoint = null;

	/**
	 * My contact, used when building packets
	 */
	private RopeContact myContact = null;

	private FingerTableCache ftCache = null;

	private CryptoCache keyCache = null;

	/**
	 * The type of request we're sending, this should be either SUCC_REQ or
	 * PRED_REQ from RopeRMIPacket.
	 */
	private int reqType = -1;

	/**
	 * The ID we're searching for.
	 */
	private RopeID targetID = null;

	/**
	 * We will store the result of the lookup here.
	 */
	private RopeContact result = null;

	private Semaphore report = null;

	private int slot = -1;

	private boolean requestSuccess = false;
	
	private MaxLogger logs = null;

	public static final int PRED = 1;
	public static final int SUCC = 2;

	private static final int MAX_LOOKUP_TRIES = 200;

	/**
	 * Builds a request object to complete a lookup.
	 * 
	 * @param startingPoint
	 *            - the node we start the querry through
	 * @param myContact
	 *            - my contact information
	 * @param reqType
	 *            - the type of lookup (succ or pred)
	 * @param targetID
	 *            - the ID we're trying to lookup
	 */
	public RopeRMIRequest(RopeContact startingPoint, FingerTableCache myFTCache, CryptoCache myKeyCache,
			RopeContact myContact, int reqType, RopeID targetID, MaxLogger log) {
		this.startingPoint = startingPoint;
		this.ftCache = myFTCache;
		this.keyCache = myKeyCache;
		this.myContact = myContact;
		this.reqType = reqType;
		this.targetID = targetID;
		this.logs = log;
	}

	/**
	 * Called for MULTI-THREADED.
	 * 
	 * @param startingPoint
	 * @param myFTCache
	 * @param myContact
	 * @param reqType
	 * @param targetID
	 */
	public RopeRMIRequest(RopeContact startingPoint, FingerTableCache myFTCache, CryptoCache myKeyCache,
			RopeContact myContact, int reqType, RopeID targetID, Semaphore report, int slot, MaxLogger log) {
		this.startingPoint = startingPoint;
		this.ftCache = myFTCache;
		this.keyCache = myKeyCache;
		this.myContact = myContact;
		this.reqType = reqType;
		this.targetID = targetID;
		this.report = report;
		this.slot = slot;
		this.logs = log;
	}

	public void run() {
		if (this.report == null) {
			throw new NullPointerException("request was asked to run in threaded mode, but not constructed correctly");
		}

		this.requestSuccess = this.runRequest();
		this.report.release();
	}

	public boolean isRequestSuccess() {
		return this.requestSuccess;
	}

	public int getSlot() {
		return this.slot;
	}

	/**
	 * Runs the full lookup, this function will return when it either meets an
	 * error, or completes the lookup. Currently the lookup is ran in an
	 * iterative manner.
	 * 
	 * @return true if the lookup completed successfully, false if there was an
	 *         error
	 */
	public boolean runRequest() {
		RopeContact currPoint = null;
		RopeContact currResult = this.startingPoint;
		int sanityCounter = 0;

		try {
			/*
			 * Send a lookup to the current node, update the current node to the
			 * result, we do this until we find a node that claims it is the
			 * result. This loop can terminate early if there is an error, in
			 * which case the function returns false.
			 */
			while (currPoint == null || !currPoint.getTheID().equals(currResult.getTheID())) {
				if (sanityCounter >= RopeRMIRequest.MAX_LOOKUP_TRIES) {
					System.err.println("would have been inf loop");
					this.result = null;
					this.logs.write("failing lookup to: " + this.targetID);
					return false;
				}

				/*
				 * Open a connection to last round's result
				 */
				currPoint = currResult;

				//this.logs.write("before fetch bool: " + currPoint + " I am " + this.myContact + " I really am " + this.ftCache.getMe());
				boolean fetchBool = this.ftCache.shouldAttemptFetch(currPoint); 
				//this.logs.write("after fetch bool: " + currPoint + " I am " + this.myContact + " I really am " + this.ftCache.getMe());
				
				if (fetchBool) {
					if (!currPoint.equals(this.myContact)) {
						/*
						 * timing info
						 */
						long startTime = System.currentTimeMillis();
						
						Socket ftSocket = new Socket(currPoint.getTheIP(), currPoint.getPort());
						OutputStream ftOutBuff = ftSocket.getOutputStream();

						/*
						 * Send request for the current node's finger table
						 */
						RopeRMIPacket ftReqPacket = RopeRMIPacket.buildOutgoingPacket(this.myContact,
								RopeRMIPacket.FT_REQ);
						ftReqPacket.setFTReqTimeStamp(this.ftCache.getLastCache(currPoint));
						ftOutBuff.write(ftReqPacket.toBytes());

						/*
						 * Parse the response, see if there is a new FT sent
						 * along, if so cache it
						 */
						RopeRMIPacket ftRespPacket = RopeRMIPacket.parseIncPacket(ftSocket);
						ftSocket.close();
						if (ftRespPacket == null) {
							this.ftCache.purgeTable(currPoint);
							this.ftCache.reportLookupFailed(currPoint);
							this.logs.write("invalid packet sent during lookup");
							this.result = null;
							this.logs.write("failing lookup to: " + this.targetID);
							return false;
						}
						if (ftRespPacket.containsNewFT()) {
							HashMap<RopeContact, byte[]> sigs = new HashMap<RopeContact, byte[]>();
							FingerTable sentFT = ftRespPacket.getFTReqRes(sigs);
							byte[] sentFTHash = sentFT.getHash();

							int validCount = 0;
							for (RopeContact tSigner : sigs.keySet()) {
								if (!this.keyCache.hasKey(tSigner)) {
									try {
										RopeRMIPacket keyReqPacket = RopeRMIPacket.buildOutgoingPacket(this.myContact,
												RopeRMIPacket.KEY_REQ);
										Socket keySocket = new Socket(tSigner.getTheIP(), tSigner.getPort());
										OutputStream out = keySocket.getOutputStream();
										out.write(keyReqPacket.toBytes());
										RopeRMIPacket keyResPacket = RopeRMIPacket.parseIncPacket(keySocket);
										this.keyCache.cacheKey(tSigner, keyResPacket.getCryptoKey());
										keySocket.close();
									} catch (IOException f) {
										this.logs.write("error asking for key of: " + tSigner);
										continue;
									}
								}

								if (this.keyCache.sigValid(tSigner, sentFTHash, sigs.get(tSigner))) {
									validCount++;
								}
							}

							//this.logs.write("VALID COUNT = " + validCount);

							if (validCount < FingerTable.VALID_THRESH) {
								this.logs.write("ft from: " + sentFT.getTableOwner() + " only has: " + validCount
										+ " sigs (I am: " + this.myContact + ")");
							}

							//FIXME at some point reject FTs if not signed, in above if statement
							this.ftCache.cacheFT(sentFT);
							this.logs.write("FT fetch took: " + (System.currentTimeMillis() - startTime));
						} else {
							this.ftCache.reportLookupFailed(currPoint);
						}
					}
				}

				FingerTable currLookupFT = this.ftCache.getCachedFT(currPoint);
				if (currLookupFT == null) {
					this.logs.write("still no FT after request to " + currPoint);
					this.logs.write("failing lookup to: " + this.targetID);
					this.result = null;
					return false;
				}

				/*
				 * Update the result for this round to the response
				 */
				if (this.reqType == RopeRMIRequest.SUCC) {
					currResult = currLookupFT.getSuccessor(this.targetID);
				} else {
					currResult = currLookupFT.getPredacessor(this.targetID);
				}
				sanityCounter++;
			}

			/*
			 * We've made it all the way through, awesome, this means we found a
			 * node that says it is the result, set it as the result, and return
			 */
			this.result = currResult;
		} catch (IOException e) {
			/*
			 * There was an error communicating with the remote node. Currently
			 * this will kill the lookup, yell about it, and fail gracefully.
			 */
			//XXX do we want to try and work around this in the future?
			this.logs.write("error connecting to: " + currPoint);
			this.ftCache.purgeTable(currPoint);
			this.ftCache.reportLookupFailed(currPoint);
			this.result = null;
			this.logs.write("failing lookup to: " + this.targetID);
			return false;
		}

		return true;
	}

	/**
	 * Fetches what is stored in the result variable. This is setup in this way
	 * for the future when we will want to multi-thread this.
	 * 
	 * @return - the contact of the result, or null if our lookup failed
	 */
	public RopeContact getResult() {
		return this.result;
	}
}
