package rope.priv.core;

import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.util.*;

import rope.io.ByteOps;
import rope.pub.data.*;
import rope.priv.data.*;
import shadowcrypto.RSAPK;

/**
 * Class used to deal with the building/parsing of packets. Instances of this
 * class should be created via the two factor methods. This class does not
 * actually map to TCP packets, this is an application level packet that can be
 * as segmented as it wants to be.
 * 
 */
//TODO at some point we should put some more "don't do outgoing things to incoming packets" checks
public class RopeRMIPacket {

	/**
	 * The contact associated with this packet. If this packet is incoming, it
	 * will be the contact information of the remote node, if it's outgoing it
	 * should be our contact information.
	 */
	private RopeContact packetContact;

	/**
	 * The packet type, it should be one of the declared constants from bellow.
	 */
	private byte type;

	/**
	 * Any non-standard data that is attached to the packet. For example a
	 * response to a query.
	 */
	private byte[] attatchedData = null;
	
	/*
	 * All of the different packet types.
	 */
	public static final byte SIG_RES = 0x01;
	public static final byte SIG_ANNOUNCE_REQ = 0x02;
	public static final byte SIG_FULL_REQ = 0X03;
	public static final byte SIG_STAB_REQ = 0x04;
	public static final byte ANNOUNCE = 0x05;
	public static final byte PING = 0x06;
	public static final byte PONG = 0x07;
	public static final byte KEY_REQ = 0x0B;
	public static final byte KEY_RES = 0x0C;
	public static final byte FT_REQ = 0x0D;
	public static final byte FT_RES = 0x0E;

	/*
	 * Constants used to build where data is in the packet
	 */
	//4 bytes: stores the size of the remaining data
	//bytes containing source ID
	//4 bytes: containing source port
	//1 byte: containing packet type
	//possible optional data....
	private static final int ID_START = 0;
	private static final int ID_SIZE = RopeID.ID_BYTE_LEN;
	private static final int PORT_START = ID_SIZE;
	private static final int PORT_SIZE = 4;
	private static final int TYPE_START = ID_SIZE + PORT_SIZE;
	private static final int TYPE_SIZE = 1;
	
	/*
	 * Min size of the packet NOT counting the size bytes
	 */
	private static final int MIN_SIZE = ID_SIZE + PORT_SIZE + TYPE_SIZE;
	
	/**
	 * Takes an incoming socket, and reads a Rope packet out of it.
	 * 
	 * @param incConnection
	 *            - the socket we want to read a packet from
	 * @return - the next full rope packet pulled from the wire, null if there
	 *         is an error (non-IO related)
	 * @throws IOException
	 *             - if there is an IO issue
	 */
	public static RopeRMIPacket parseIncPacket(Socket incConnection) throws IOException {

		/*
		 * Fetch input stream
		 */
		InputStream incBuff = incConnection.getInputStream();

		/*
		 * Prep buffers, get packet size
		 */
		byte[] sizeWord = new byte[4];
		for (int counter = 0; counter < 4; counter++) {
			sizeWord[counter] = (byte) incBuff.read();
		}
		int readSize = 0;
		int reqSize = ByteOps.wordToInt(sizeWord);
		byte[] packetData = new byte[reqSize];
		byte[] readArray = new byte[reqSize];

		/*
		 * Packet size sanity check based on reported packet size
		 */
		if (reqSize < RopeRMIPacket.MIN_SIZE) {
			System.err.println("invalid packet size");
			return null;
		}

		/*
		 * Read packet untill we have all the data we were told we would have
		 */
		while (readSize < reqSize) {
			int tempReadAmount = incBuff.read(readArray);
			for (int counter = 0; counter < tempReadAmount; counter++) {
				packetData[counter + readSize] = readArray[counter];
			}
			readSize += tempReadAmount;
		}

		/*
		 * Build packet contact
		 */
		byte[] idBytes = ByteOps.subArray(packetData, ID_START, ID_SIZE);
		byte[] portBytes = ByteOps.subArray(packetData, PORT_START, PORT_SIZE);
		byte typeByte = ByteOps.subArray(packetData, TYPE_START, TYPE_SIZE)[0];
		RopeContact incContact = new RopeContact(new RopeID(idBytes), incConnection.getInetAddress(), ByteOps
				.wordToInt(portBytes));
		RopeRMIPacket newPacket = new RopeRMIPacket(incContact, typeByte);

		/*
		 * Attach the added data, which is simply any data not part of the
		 * header
		 */
		if (reqSize > RopeRMIPacket.MIN_SIZE) {
			newPacket.setAttatchedData(ByteOps.subArray(packetData, RopeRMIPacket.MIN_SIZE, reqSize
					- RopeRMIPacket.MIN_SIZE));
		}

		/*
		 * Return packet object
		 */
		return newPacket;
	}

	/**
	 * Builds an outgoing packet without any attatched data.
	 * 
	 * @param myContact
	 *            - the local contact, used to ID the node
	 * @param packetType
	 *            - the type of packet to send
	 * @return - a packet with the correct fields
	 */
	public static RopeRMIPacket buildOutgoingPacket(RopeContact myContact, byte packetType) {
		return new RopeRMIPacket(myContact, packetType);
	}

	/**
	 * Internal constructor used, sets the contact and type only.
	 * 
	 * @param packetContact
	 *            - the packet contact
	 * @param packetType
	 *            - the packet type
	 */
	private RopeRMIPacket(RopeContact packetContact, byte packetType) {
		this.packetContact = packetContact;
		this.type = packetType;
	}

	/**
	 * Attaches raw data to an outgoing packet. This is used internally only,
	 * everyone else needs to go through the correct call to ensure they are not
	 * doing anything stupid.
	 * 
	 * @param data
	 *            - data we want to send
	 */
	private void setAttatchedData(byte[] data) {
		this.attatchedData = data;
	}

	public long getFTReqTimeStamp() {
		if (this.type != RopeRMIPacket.FT_REQ) {
			System.err.println("asked for ft request time stamp in a different type of packet");
			return -1;
		}

		return ByteOps.wordToLong(this.attatchedData);
	}

	public void setFTReqTimeStamp(long time) {
		if (this.type != RopeRMIPacket.FT_REQ) {
			System.err.println("asked to pack a timestamp in a non-FT request packet");
		}
		this.attatchedData = ByteOps.longToWord(time);
	}

	public void setFTReqResFT(long ts, FingerTable ft) {
		if (this.type != RopeRMIPacket.FT_RES) {
			System.err.println("asked to pack a finger table in a non-FT response packet");
		}

		this.attatchedData = ByteOps.longToWord(ts);
		if (ft != null) {
			HashMap<RopeContact, byte[]> sigMap = ft.getSigs();
			this.attatchedData = ByteOps.catByteArrays(this.attatchedData, ByteOps.intToWord(sigMap.size()));
			for (RopeContact tContact : sigMap.keySet()) {
				this.attatchedData = ByteOps.catByteArrays(this.attatchedData, ByteOps.catByteArrays(RopeContact
						.getBytes(tContact), sigMap.get(tContact)));
			}
			this.attatchedData = ByteOps.catByteArrays(this.attatchedData, ft.dumpTableToBytes());
		}
	}

	public boolean containsNewFT() {
		if (this.type != RopeRMIPacket.FT_RES) {
			System.err.println("asked about FT response in wrong type of packet");
			return false;
		}

		return this.attatchedData.length > 8;
	}

	public FingerTable getFTReqRes(HashMap<RopeContact, byte[]> sigMap) {
		if (this.type != RopeRMIPacket.FT_RES) {
			System.err.println("asked for FT in wrong type of packet");
			return null;
		}

		FingerTable retTable = null;
		try {
			int sigCount = ByteOps.wordToInt(ByteOps.subArray(this.attatchedData, 8, 4));
			for (int counter = 0; counter < sigCount; counter++) {
				sigMap.put(RopeContact.parseBytesToContact(ByteOps.subArray(this.attatchedData, 12
						+ (RopeContact.BYTE_SIZE + FingerTable.SIG_SIZE) * counter, RopeContact.BYTE_SIZE)), ByteOps
						.subArray(this.attatchedData, 12 + RopeContact.BYTE_SIZE
								+ (RopeContact.BYTE_SIZE + FingerTable.SIG_SIZE) * counter, FingerTable.SIG_SIZE));
			}

			int sigSize = (RopeContact.BYTE_SIZE + FingerTable.SIG_SIZE) * sigCount;
			byte[] ftBytes = ByteOps.subArray(this.attatchedData, 12 + sigSize, this.attatchedData.length
					- (12 + sigSize));

			/*
			 * check the status of sigs
			 */
//			if (sigMap.size() < FingerTable.VALID_THRESH) {
//				RopeRMIPacket.logs.write("not presented with enough valid sigs from: " + this.packetContact);
//				//return null;
//			} 

			retTable = new FingerTable(ftBytes);			
		} catch (UnknownHostException e) {
			e.printStackTrace();
			retTable = null;
		}

		return retTable;
	}

	/**
	 * Sets the querry response for a packet.
	 * 
	 * @param resContact
	 *            - the local result of a query
	 */
	public void setReqResContact(RopeContact resContact) {
		this.attatchedData = RopeContact.getBytes(resContact);
	}

	public void setAnnounceSigReqData(RopeContact incNode, byte[] oldFT, byte[] newFT) {
		this.attatchedData = ByteOps.catByteArrays(RopeContact.getBytes(incNode), ByteOps.catByteArrays(ByteOps
				.intToWord(oldFT.length), ByteOps.intToWord(newFT.length)));
		this.attatchedData = ByteOps.catByteArrays(this.attatchedData, ByteOps.catByteArrays(oldFT, newFT));
	}

	public RopeContact getAnnounceSigJoiningNode() throws UnknownHostException {
		return RopeContact.parseBytesToContact(ByteOps.subArray(this.attatchedData, 0, RopeContact.BYTE_SIZE));
	}

	public FingerTable getAnnounceSigOldFT() throws UnknownHostException {
		int oldSize = ByteOps.wordToInt(ByteOps.subArray(this.attatchedData, RopeContact.BYTE_SIZE, 4));
		byte[] oldBytes = ByteOps.subArray(this.attatchedData, RopeContact.BYTE_SIZE + 8, oldSize);
		return new FingerTable(oldBytes);
	}

	public FingerTable getAnnounceSigNewFT() throws UnknownHostException {
		int oldSize = ByteOps.wordToInt(ByteOps.subArray(this.attatchedData, RopeContact.BYTE_SIZE, 4));
		int newSize = ByteOps.wordToInt(ByteOps.subArray(this.attatchedData, RopeContact.BYTE_SIZE + 4, 4));
		byte[] newBytes = ByteOps.subArray(this.attatchedData, RopeContact.BYTE_SIZE + 8 + oldSize, newSize);
		return new FingerTable(newBytes);
	}

	public void setStabSigReqFT(FingerTable ft){
		this.attatchedData = ft.dumpTableToBytes();
	}
	
	public FingerTable getStabSigReqFT() throws UnknownHostException{
		return new FingerTable(this.attatchedData);
	}

	public void setSigRes(byte[] hash, byte[] newSig) {
		if (this.type != RopeRMIPacket.SIG_RES) {
			System.err.println("asked to pack a sig in a non sig response packet");
			return;
		}

		this.setAttatchedData(ByteOps.catByteArrays(newSig, hash));
	}

	public byte[] getSigSigRes() {
		if (this.type != RopeRMIPacket.SIG_RES) {
			System.err.println("asked to get sig from a non sig response packet");
			return null;
		}

		return ByteOps.subArray(this.attatchedData, 0, FingerTable.SIG_SIZE);
	}

	public byte[] getHashSigRes() {
		if (this.type != RopeRMIPacket.SIG_RES) {
			System.err.println("asked to get hash from a non sig response packet");
			return null;
		}

		return ByteOps.subArray(this.attatchedData, FingerTable.SIG_SIZE, this.attatchedData.length
				- FingerTable.SIG_SIZE);
	}

	public void setFullSigReqTS(long ts) {
		if (this.type != RopeRMIPacket.SIG_FULL_REQ && this.type != RopeRMIPacket.SIG_STAB_REQ) {
			System.err.println("asked to store full sig req ts in wrong packet type");
			return;
		}
		this.setAttatchedData(ByteOps.longToWord(ts));
	}

	public long getFullSigReqTS() {
		if (this.type != RopeRMIPacket.SIG_FULL_REQ) {
			System.err.println("asked for full sig req ts in wrong packet type");
			return -1;
		}
		return ByteOps.wordToLong(this.attatchedData);
	}
	
	public void packCryptoKey(RSAPK theKey){
		byte[] e = theKey.getE().toByteArray();
		byte[] n = theKey.getN().toByteArray();
		this.attatchedData = ByteOps.catByteArrays(ByteOps.intToWord(e.length), ByteOps.intToWord(n.length));
		this.attatchedData = ByteOps.catByteArrays(this.attatchedData, ByteOps.catByteArrays(e, n));
	}
	
	public RSAPK getCryptoKey(){
		int eLen = ByteOps.wordToInt(ByteOps.subArray(this.attatchedData, 0, 4));
		byte[] e = ByteOps.subArray(this.attatchedData, 8, eLen);
		byte[] n = ByteOps.subArray(this.attatchedData, 8 + eLen, ByteOps.wordToInt(ByteOps.subArray(this.attatchedData, 4, 4)));
		return new RSAPK(new BigInteger(e), new BigInteger(n));
	}

	/**
	 * Builds a byte array to be sent over the wire. This actually does building
	 * and allocating of memory, so this should not be called freely, store the
	 * result. This byte array can not simply be sent to the destination, first
	 * a byte storing the size of this object must be sent.
	 * 
	 * @return - a byte array to be pushed to the wire
	 */
	public byte[] toBytes() {
		/*
		 * Start with packet type
		 */
		byte[] typeArr = new byte[1];
		typeArr[0] = this.type;

		/*
		 * Add in the contact information
		 */
		byte[] wireBytes = this.packetContact.getTheID().getBytes();
		wireBytes = ByteOps.catByteArrays(wireBytes, ByteOps.intToWord(this.packetContact.getPort()));
		wireBytes = ByteOps.catByteArrays(wireBytes, typeArr);

		/*
		 * Attatch the optional data if any, and we're done
		 */
		if (this.attatchedData != null) {
			wireBytes = ByteOps.catByteArrays(wireBytes, this.attatchedData);
		}

		byte[] sizeWord = ByteOps.intToWord(wireBytes.length);
		wireBytes = ByteOps.catByteArrays(sizeWord, wireBytes);
		return wireBytes;
	}

	/**
	 * Gets the packet contact for this packet. This really should only be
	 * useful for incoming packets, as all outgoing packets will have your own
	 * contact information as the contact.
	 * 
	 * @return - the contact associated with the packet
	 */
	public RopeContact getPacketContact() {
		return this.packetContact;
	}

	/**
	 * Returns packet type
	 * 
	 * @return - the type byte associated with the packet
	 */
	public byte getType() {
		return this.type;
	}

	public String toString() {
		return this.packetContact.toString();
	}
}
