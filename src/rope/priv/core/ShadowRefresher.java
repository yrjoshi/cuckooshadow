package rope.priv.core;

import java.util.concurrent.Semaphore;

import rope.pub.data.*;
import rope.priv.data.*;

public class ShadowRefresher implements Runnable {

	private RopeID baseNode = null;

	private RopeContact replyNode = null;

	private RopeContact originatingNode = null;

	private RopeContact[] result = null;

	private FingerTable lookupTable = null;

	private FingerTableCache ftCache = null;
	
	private CryptoCache keyCache = null;

	private boolean isSuccessor = false;

	private Semaphore wall = null;
	
	private MaxLogger logs = null;

	public ShadowRefresher(RopeID baseNode, RopeContact replyNode, RopeContact originatingNode,
			FingerTable lookupTable, FingerTableCache ftCache, CryptoCache cryptCache, boolean isSuccessor, Semaphore wall, MaxLogger log) {
		super();
		this.baseNode = baseNode;
		this.replyNode = replyNode;
		this.originatingNode = originatingNode;
		this.lookupTable = lookupTable;
		this.ftCache = ftCache;
		this.keyCache = cryptCache;
		this.isSuccessor = isSuccessor;
		this.wall = wall;
		this.logs = log;

		this.result = new RopeContact[FingerTable.SUCC_COUNT];
	}

	public RopeID getBaseNode(){
		return this.baseNode;
	}
	
	public RopeContact[] getResult(){
		return this.result;
	}
	
	public boolean isSuccessor(){
		return this.isSuccessor;
	}
	
	public void run() {

		RopeID currNode = this.baseNode;
		RopeRMIRequest request;
		boolean lookupError = false;
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {

			if (this.isSuccessor) {
				currNode = currNode.incrimentID();
			}

			if (this.isSuccessor) {
				request = new RopeRMIRequest(this.lookupTable.getSuccessor(currNode), this.ftCache, this.keyCache, this.ftCache.getMe(),
						RopeRMIRequest.SUCC, currNode, this.logs);
			} else {
				request = new RopeRMIRequest(this.lookupTable.getPredacessor(currNode), this.ftCache, this.keyCache, this.ftCache.getMe(),
						RopeRMIRequest.PRED, currNode, this.logs);
			}

			if (request.runRequest()) {
				RopeContact resp = request.getResult();
				//this.logs.write("YARRR: " + resp);
				
				/*
				 * Sanity check to prevent us from adding a node as it's own
				 * shadow
				 */
				if (resp.getTheID().equals(this.baseNode)) {
					break;
				}

				this.result[counter] = resp;
				currNode = resp.getTheID();
			} else {
				lookupError = true;
				break;
			}
		}

		/*
		 * If there is an error, don't mess with trying to predict if we're a
		 * shadow
		 */
		if (lookupError) {
			this.logs.write("incomplete build of shadows for: " + this.baseNode + " at " + this.ftCache.getMe());
			this.wall.release();
			//this.logs.write("RELEASING: " + this.ftCache.getMe());
			return;
		}
		
		//this.logs.write("After lookup: " + this.ftCache.getMe());

		/*
		 * No need to do the bullshit "see if we need to add ourselves" stuff if
		 * the node is us
		 */
		if (this.originatingNode.getTheID().equals(this.baseNode)) {
			this.wall.release();
			//this.logs.write("RELEASING: " + this.ftCache.getMe());
			return;
		}

		/*
		 * On bootstrap we need to check and see if we would be a better shadow
		 * for someone, and should add ourself
		 */
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			RopeID prev = null;
			RopeID next = null;

			if (this.result[counter] == null) {
				this.result[counter] = this.originatingNode;
				break;
			}

			if (counter == 0) {
				prev = this.baseNode;
			} else {
				prev = this.result[counter - 1].getTheID();
			}
			next = this.result[counter].getTheID();

			if ((this.isSuccessor && this.originatingNode.getTheID().betweenExclusive(next, prev))
					|| (!this.isSuccessor && this.originatingNode.getTheID().betweenExclusive(prev, next))) {
				for (int moveCounter = FingerTable.SUCC_COUNT - 2; moveCounter >= counter; moveCounter--) {
					this.result[moveCounter + 1] = this.result[moveCounter];
				}
				this.result[counter] = this.originatingNode;
				break;
			}
		}

		this.wall.release();
		//this.logs.write("RELEASING: " + this.ftCache.getMe());
		return;
	}

}
