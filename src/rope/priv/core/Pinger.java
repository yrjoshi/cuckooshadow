package rope.priv.core;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.Semaphore;

import rope.pub.data.*;

public class Pinger implements Runnable {

	private RopeContact me = null;
	
	private RopeContact target = null;
	
	private boolean alive = false;
	
	private Semaphore notify = null;
	
	private int position = -1;
	
	private boolean successor = false;
	
	private MaxLogger logs = null;
	
	public Pinger(RopeContact me, RopeContact target, Semaphore notify, int position, boolean successor, MaxLogger log) {
		super();
		this.me = me;
		this.target = target;
		this.notify = notify;
		this.position = position;
		this.successor = successor;
		this.logs = log;
	}

	public boolean isAlive(){
		return this.alive;
	}

	public boolean isSuccessor(){
		return this.successor;
	}
	
	public int getPosition(){
		return this.position;
	}
	
	public void run() {
		
		try {
			Socket pingSocket = new Socket(this.target.getTheIP(), this.target.getPort());
			RopeRMIPacket pingPacket = RopeRMIPacket.buildOutgoingPacket(this.me, RopeRMIPacket.PING);
			byte[] packetBytes = pingPacket.toBytes();

			OutputStream out = pingSocket.getOutputStream();
			out.write(packetBytes);

			RopeRMIPacket pongPacket = RopeRMIPacket.parseIncPacket(pingSocket);
			if (pongPacket.getType() != RopeRMIPacket.PONG) {
				this.logs.write("bad pong response from: " + this.target);
				this.alive = false;
				this.notify.release();
				return;
			}
			pingSocket.close();
		} catch (IOException e) {
			this.logs.write("error trying to ping: " + this.target);
			this.alive = false;
			this.notify.release();
			return;
		}

		this.alive = true;
		this.notify.release();
		return;	
	}

}
