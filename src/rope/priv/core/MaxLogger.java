package rope.priv.core;

import java.io.*;

public class MaxLogger {

	private BufferedWriter logFile = null;

	public MaxLogger(String logFileName) throws IOException {
		this.logFile = new BufferedWriter(new FileWriter(logFileName));
	}

	public void write(String logMessage) {
		try {
			this.logFile.write(logMessage + "\n");
			this.logFile.flush();
			//System.out.println(logMessage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void doneLogging() {
		try {
			this.logFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
