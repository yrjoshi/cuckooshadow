package rope.priv.core;

public class SuicideBomb implements Runnable {

	private long timeToLive = -1;
	private boolean armed = false;
	private MaxLogger log = null;
	
	public SuicideBomb(long ttl, MaxLogger logFile){
		this.timeToLive = ttl;
		this.armed = true;
		this.log = logFile;
	}
	
	public void run() {
		
		try {
			Thread.sleep(this.timeToLive);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if(this.armed){
			this.log.write("killing myself");
			this.log.doneLogging();
			System.exit(-100);
		}
	}
	
	public void disarm(){
		this.armed = false;
	}

}
