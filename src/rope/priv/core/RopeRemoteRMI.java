package rope.priv.core;

import java.io.*;
import java.net.*;
import java.util.*;

import rope.priv.data.*;
import rope.pub.data.*;
import rope.pub.core.*;

/**
 * Class that deals with a request from a remote host. This currently can be a
 * successor or predecessor request, a ping/pong, an announce, or a notification
 * of dead predecessor. This should be spun off into a thread to avoid blocking.
 * 
 */
public class RopeRemoteRMI implements Runnable {

	/**
	 * The socket the incoming connection is on.
	 */
	private Socket theConnection = null;

	/**
	 * My contact information, used to build the correct data into packets
	 */
	private RopeContact myContact = null;

	/**
	 * My finger table, used to generate responses to lookups, might be changed
	 * in the case of announce/dead pred
	 */
	private FingerTable myFingers = null;

	private FingerTableCache myCache = null;

	private CryptoCache myCryptoCache = null;
	
	private MaxLogger logs = null;

	private RopeNode myNode = null;

	/**
	 * Builds a remote rmi instance to handle one remote method invocation.
	 * 
	 * @param incConn
	 *            - the socket to the requester
	 * @param myContact
	 *            - my contact information
	 * @param myFingers
	 *            - my finger table
	 */
	public RopeRemoteRMI(Socket incConn, RopeContact myContact, FingerTable myFingers, FingerTableCache myCache,
			CryptoCache myCryptoCache, RopeNode myNode, MaxLogger log) {
		this.theConnection = incConn;
		this.myContact = myContact;
		this.myFingers = myFingers;
		this.myCache = myCache;
		this.myCryptoCache = myCryptoCache;
		this.myNode = myNode;
		this.logs = log;
	}

	/**
	 * Deals with the RMI and then closes.
	 */
	public void run() {
		RopeRMIPacket incPacket = null;
		RopeRMIPacket respPacket = null;

		/*
		 * Read the data and build the RMI packet object
		 */
		try {
			incPacket = RopeRMIPacket.parseIncPacket(this.theConnection);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		/*
		 * If we have a valid packet do stuff with it
		 */
		if (incPacket != null) {
			if (incPacket.getType() == RopeRMIPacket.ANNOUNCE) {
				/*
				 * A new node thinks we might want it as a finger, offer it to
				 * the finger table, no response needed.
				 */
				this.logs.write("got announce from: " + incPacket.getPacketContact());
				RopeContact joiningContact = incPacket.getPacketContact();
				if (this.myFingers.willResultInChange(joiningContact)) {
					RopeContact[][] newShadows = this.runShadowRefresh(incPacket.getPacketContact().getTheID());

					this.myNode.createFetus();
					this.myNode.getFetus().offerFinger(incPacket.getPacketContact());
					this.myNode.getFetus().setFingerShadows(incPacket.getPacketContact().getTheID(), newShadows[1],
							newShadows[0]);
					this.myNode.getFetus().markAsUpdated();

					byte[] oldFT = this.myFingers.dumpTableToBytes();
					byte[] newFT = this.myNode.getFetus().dumpTableToBytes();

					//this.logs.write("looking for ANNOUNCE sig on:\n" + this.myNode.getFetus());

					RopeRMIPacket sigReq = RopeRMIPacket.buildOutgoingPacket(this.myContact,
							RopeRMIPacket.SIG_ANNOUNCE_REQ);
					sigReq.setAnnounceSigReqData(joiningContact, oldFT, newFT);
					byte[] outBytes = sigReq.toBytes();

					HashSet<RopeContact> myShadows = this.myFingers.getMyShadows();
					for (RopeContact tContact : myShadows) {
						try {
							Socket tSocket = new Socket(tContact.getTheIP(), tContact.getPort());
							OutputStream tOut = tSocket.getOutputStream();
							tOut.write(outBytes);
							tSocket.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			} else if (incPacket.getType() == RopeRMIPacket.PING) {
				/*
				 * Some node is testing to see if we're alive, send a ping
				 */
				//XXX it might be nice if this had a nonce or something in it, just so we can echo that back
				respPacket = RopeRMIPacket.buildOutgoingPacket(this.myContact, RopeRMIPacket.PONG);
			} else if (incPacket.getType() == RopeRMIPacket.FT_REQ) {
				//this.logs.write("got ft req: " + this.myContact);
				/*
				 * The other side asked for our FT, check to see when their copy
				 * is from, it ours is newer, give them the new one, otherwise
				 * tell them that nothing has changed
				 */
				long remoteTime = incPacket.getFTReqTimeStamp();
				//this.logs.write("they have: " + remoteTime + " I have " + this.myFingers.getLastUpdated());
				respPacket = RopeRMIPacket.buildOutgoingPacket(this.myContact, RopeRMIPacket.FT_RES);
				if (remoteTime != this.myFingers.getLastUpdated()) {
					respPacket.setFTReqResFT(this.myFingers.getLastUpdated(), this.myFingers);
				} else {
					respPacket.setFTReqResFT(this.myFingers.getLastUpdated(), null);
				}
			} else if (incPacket.getType() == RopeRMIPacket.SIG_ANNOUNCE_REQ) {
				RopeContact joiningContact = null;
				FingerTable oldFT = null;
				FingerTable newFT = null;

				try {
					joiningContact = incPacket.getAnnounceSigJoiningNode();
					oldFT = incPacket.getAnnounceSigOldFT();
					newFT = incPacket.getAnnounceSigNewFT();
				} catch (UnknownHostException e) {
					//XXX not really sure I like this error handling, but low priority
					e.printStackTrace();
					return;
				}

				RopeContact[][] tempFingers = this.runShadowRefresh(joiningContact.getTheID());

				oldFT.offerFinger(joiningContact);
				oldFT.setFingerShadows(joiningContact.getTheID(), tempFingers[1], tempFingers[0]);
//				if (oldFT.equals(newFT)) {
					//this.logs.write("signing:\n" + newFT);

					RopeRMIPacket outPacket = RopeRMIPacket.buildOutgoingPacket(this.myContact, RopeRMIPacket.SIG_RES);
					byte[] hash = newFT.getHash();
					byte[] sig = this.myCryptoCache.sign(hash);
					outPacket.setSigRes(hash, sig);
					try {
						Socket tSocket = new Socket(incPacket.getPacketContact().getTheIP(), incPacket
								.getPacketContact().getPort());
						OutputStream tOut = tSocket.getOutputStream();
						tOut.write(outPacket.toBytes());
						tSocket.close();
					} catch (IOException e) {
						this.logs.write("error trying to send sig");
					}
//				} else {
//					this.logs.write("nnooooo");
//					this.logs.write("conflict a: " + oldFT);
//					this.logs.write("conflict b: " + newFT);
//				}
			} else if (incPacket.getType() == RopeRMIPacket.SIG_RES) {
				this.myNode.postSig(incPacket.getHashSigRes(), incPacket.getPacketContact(), incPacket.getSigSigRes(),
						false);
			} else if (incPacket.getType() == RopeRMIPacket.SIG_FULL_REQ) {
				FingerTable otherFT = new FingerTable(incPacket.getPacketContact(), incPacket.getFullSigReqTS());
				RopeLocalRMI localDaemon = new RopeLocalRMI(otherFT.getTableOwner(), otherFT, this.myCache,
						this.myCryptoCache, RopeLocalRMI.BUILD_OTHERFT, this.myFingers, this.logs);
				Thread tThread = new Thread(localDaemon);
				tThread.start();
			} else if (incPacket.getType() == RopeRMIPacket.SIG_STAB_REQ) {
				try {
					FingerTable otherFT = incPacket.getStabSigReqFT();
					RopeLocalRMI localDaemon = new RopeLocalRMI(otherFT.getTableOwner(), otherFT, this.myCache,
							this.myCryptoCache, RopeLocalRMI.STABALIZE_OTHERFT, this.myFingers, this.logs);
					Thread tThread = new Thread(localDaemon);
					tThread.start();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
			} else if (incPacket.getType() == RopeRMIPacket.KEY_REQ) {
				respPacket = RopeRMIPacket.buildOutgoingPacket(this.myContact, RopeRMIPacket.KEY_RES);
				respPacket.packCryptoKey(this.myCryptoCache.getMyPubKey());
			} else {
				this.logs.write("Unknown packet type");
			}
		} else {
			/*
			 * We recieved a malformed packet, yell and move on w/ life
			 */
			this.logs.write("recieved bad packet for RMI from: " + this.theConnection.toString());
		}

		/*
		 * If we have a response to send, we should do it. As allways, the first
		 * byte is the packet size, then the packet itself.
		 */
		if (respPacket != null) {
			try {
				byte[] outBytes = respPacket.toBytes();
				this.theConnection.getOutputStream().write(outBytes);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/*
		 * Close the socket, return
		 */
		try {
			this.theConnection.close();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}

	private RopeContact[][] runShadowRefresh(RopeID startingID) {
		RopeContact shadows[][] = new RopeContact[2][FingerTable.SUCC_COUNT];
		RopeID currSucc = startingID;
		RopeRMIRequest request;
		boolean lookupError = false;
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			request = new RopeRMIRequest(this.myFingers.getSuccessor(currSucc), this.myCache, this.myCryptoCache, this.myContact,
					RopeRMIRequest.SUCC, currSucc, this.logs);

			if (request.runRequest()) {
				RopeContact resp = request.getResult();
				if (resp.getTheID().equals(startingID)) {
					break;
				}

				shadows[0][counter] = resp;
				currSucc = resp.getTheID();
			} else {
				lookupError = true;
				break;
			}
		}

		RopeID currPred = startingID;
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			request = new RopeRMIRequest(this.myFingers.getPredacessor(currPred), this.myCache, this.myCryptoCache,this.myContact,
					RopeRMIRequest.PRED, currPred, this.logs);
			if (request.runRequest()) {
				RopeContact resp = request.getResult();
				if (resp.getTheID().equals(startingID)) {
					break;
				}

				shadows[1][counter] = resp;
				currPred = resp.getTheID();
			} else {
				lookupError = true;
				break;
			}
		}

		/*
		 * If there is an error, don't mess with trying to predict if we're a
		 * shadow
		 */
		if (lookupError) {
			return shadows;
		}

		if (this.myContact.getTheID().equals(startingID)) {
			return shadows;
		}

		/*
		 * On bootstrap we need to check and see if we would be a better shadow
		 * for someone, and should add ourself
		 */
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			RopeID prev = null;
			RopeID next = null;

			if (shadows[0][counter] == null) {
				shadows[0][counter] = this.myContact;
				break;
			}

			if (counter == 0) {
				prev = startingID;
			} else {
				prev = shadows[0][counter - 1].getTheID();
			}
			next = shadows[0][counter].getTheID();

			if (this.myContact.getTheID().betweenExclusive(next, prev)) {
				for (int moveCounter = FingerTable.SUCC_COUNT - 2; moveCounter >= counter; moveCounter--) {
					shadows[0][moveCounter + 1] = shadows[0][moveCounter];
				}
				shadows[0][counter] = this.myContact;
				break;
			}
		}
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			RopeID prev = null;
			RopeID next = null;

			if (shadows[1][counter] == null) {
				shadows[1][counter] = this.myContact;
				break;
			}

			if (counter == 0) {
				prev = startingID;
			} else {
				prev = shadows[1][counter - 1].getTheID();
			}
			next = shadows[1][counter].getTheID();

			if (this.myContact.getTheID().betweenExclusive(prev, next)) {
				for (int moveCounter = FingerTable.SUCC_COUNT - 2; moveCounter >= counter; moveCounter--) {
					shadows[1][moveCounter + 1] = shadows[0][moveCounter];
				}
				shadows[1][counter] = this.myContact;
				break;
			}
		}

		return shadows;
	}
}
