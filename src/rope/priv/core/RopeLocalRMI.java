package rope.priv.core;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.Semaphore;

import rope.priv.data.*;
import rope.pub.data.*;

//TODO in general double check that we are starting lookups from the most optimal point
public class RopeLocalRMI implements Runnable {

	private RopeContact myContact = null;
	private FingerTable editFingers = null;
	private FingerTableCache myFTCache = null;
	private CryptoCache myCryptoCache = null;
	private FingerTable suppFingers = null;
	private int task = -1;
	private MaxLogger logs = null;

	public static final int BOOTSTRAP = 1;
	public static final int BUILD_OTHERFT = 2;
	public static final int STABALIZE = 3;
	public static final int ANNOUNCE = 4;
	public static final int STABALIZE_OTHERFT = 5;

	private static final boolean DEBUG = false;

	public RopeLocalRMI(RopeContact myContactInfo, FingerTable editFingers, FingerTableCache myFTCache,
			CryptoCache myCryptoCache, int task, FingerTable supplimentalFingers, MaxLogger log) {
		this.myContact = myContactInfo;
		this.editFingers = editFingers;
		this.myFTCache = myFTCache;
		this.myCryptoCache = myCryptoCache;
		this.task = task;
		this.suppFingers = supplimentalFingers;
		this.logs = log;
	}

	public void run() {
		try {
			if (this.task == RopeLocalRMI.BOOTSTRAP) {

				/*
				 * Timing information
				 */
				long startTime = System.currentTimeMillis();

				/*
				 * Biuld our fingers
				 */
				byte[] id = new byte[RopeID.ID_BYTE_LEN];

				/*
				 * Flip a coin as to which well known node we go do, ghetto load
				 * balancing, but load balancing none the less
				 */
				//XXX should this be which ID is closer to us?
				Random rng = new Random();
				int port = -1;
				if (rng.nextInt() % 2 == 0) {
					id[0] = 0x00;
					port = 14000;
				} else {
					id[0] = (byte) 128;
					port = 14001;
				}

				RopeContact wkNode = null;
				//if (RopeLocalRMI.DEBUG) {
				//	wkNode = new RopeContact(new RopeID(id), InetAddress.getByName("128.101.34.147"), port);
				//} else {
					wkNode = new RopeContact(new RopeID(id), InetAddress.getByName("146.57.249.104"), port);
					//}

				/*
				 * First find our direct successor
				 */
				RopeRMIRequest firstLookup = new RopeRMIRequest(wkNode, this.myFTCache, this.myCryptoCache,
						this.myContact, RopeRMIRequest.SUCC, this.myContact.getTheID(), this.logs);
				if (firstLookup.runRequest()) {
					this.editFingers.offerFinger(firstLookup.getResult());
				} else {
					this.logs.write("COULDN'T FIND MY SUCCESSOR!");
					this.logs.doneLogging();
					System.exit(-3);
				}

				List<RopeID> fingerStarts = this.myContact.getTheID().getFingerStarts();
				int start = this.editFingers.getFirstEmptyFinger();

				if (start != -1) {
					for (int slot = start; slot < fingerStarts.size(); slot++) {
						RopeID tID = fingerStarts.get(slot);
						RopeRMIRequest request = new RopeRMIRequest(wkNode, this.myFTCache, this.myCryptoCache,
								this.myContact, RopeRMIRequest.SUCC, tID, this.logs);
						//XXX yell more during failure since it's a bootstrap?
						if (request.runRequest()) {
							this.editFingers.offerFinger(request.getResult());
						}
						else{
							this.logs.write("dies in bootstrap");
							this.logs.doneLogging();
							System.exit(-3);
						}
					}
				} else {
					this.logs.write("first node filled all!");
				}

				/*
				 * Build our predacessor and successor lists, collectively our
				 * shadows
				 */
				//XXX yell more about failed lookups in bootstrap?
				this.refreshMyShadows();
				this.refreshMyFingerShadows();
				this.editFingers.markAsUpdated();

				//this.logs.write("my first FT: " + this.editFingers.getTableOwner() + "\n" + this.editFingers);

				this.askForFullCheckSigs(false);

				/*
				 * Timing reporting
				 */
				startTime = System.currentTimeMillis() - startTime;
				this.logs.write("building first FT took: " + startTime);

			} else if (this.task == RopeLocalRMI.STABALIZE) {

				/*
				 * Timing info
				 */
				long startTime = System.currentTimeMillis();

				int skipableFingers = this.pingShadows(this.editFingers, this.myContact);

				/*
				 * Refresh each finger slot
				 */
				this.refreshFingers(this.editFingers, this.editFingers, skipableFingers);

				/*
				 * Refresh our predecessor and successor list, collectively our
				 * shadows
				 */
				//				HashSet<RopeID> shadowedSet = this.editFingers.getShadowedFingers();
				//				shadowedSet.add(this.editFingers.getMyPredSlot(0).getTheID());
				//				shadowedSet.add(this.myContact.getTheID());
				//				this.refreshShadowSet(this.editFingers, this.editFingers, this.myContact, shadowedSet);
				this.refreshMyShadows();
				this.refreshMyFingerShadows();
				this.editFingers.markAsUpdated();

				startTime = System.currentTimeMillis() - startTime;
				this.logs.write("stabalize took: " + startTime);

				if(DEBUG){
				    this.logs.write("new (stab) table for: " + this.myContact + "\n" + this.editFingers.toString());
				}

				this.askForFullCheckSigs(true);
			} else if (this.task == RopeLocalRMI.BUILD_OTHERFT) {

				/*
				 * First find our direct successor
				 */
				RopeRMIRequest firstLookup = new RopeRMIRequest(this.suppFingers.getSuccessor(this.editFingers
						.getTableOwner().getTheID()), this.myFTCache, this.myCryptoCache, this.editFingers
						.getTableOwner(), RopeRMIRequest.SUCC, this.editFingers.getTableOwner().getTheID(), this.logs);
				if (firstLookup.runRequest()) {
					this.editFingers.offerFinger(firstLookup.getResult());
				} else {
					this.logs.write("COULDN'T FIND OTHER NODE'S SUCCESSOR!");
					return;
				}

				/*
				 * Build each finger for the foreign FT
				 */
				int posToStart = this.editFingers.getFirstEmptyFinger();
				if (posToStart != -1) {
					this.refreshFingers(this.editFingers, this.suppFingers, posToStart);
				} else {
					this.logs.write("worked on first pass");
				}

				/*
				 * Build our predecessor and successor lists, collectively our
				 * shadows
				 */
				this.refreshMyShadows();
				this.refreshMyFingerShadows();

				if(DEBUG){
				    this.logs.write("built for sig: " + this.suppFingers.getTableOwner() + "\n"
						+ this.editFingers.toString() + "\nfor: " + this.myContact);
				}

				this.logs.write("sending opening sig to: " + this.myContact + " (I am: "
						+ this.suppFingers.getTableOwner() + ")");
				RopeRMIPacket outPacket = RopeRMIPacket.buildOutgoingPacket(this.suppFingers.getTableOwner(),
						RopeRMIPacket.SIG_RES);
				byte[] hash = this.editFingers.getHash();
				byte[] sig = this.myCryptoCache.sign(hash);
				outPacket.setSigRes(hash, sig);
				try {
					Socket tSocket = new Socket(this.myContact.getTheIP(), this.myContact.getPort());
					OutputStream tOut = tSocket.getOutputStream();
					tOut.write(outPacket.toBytes());
					tSocket.close();
				} catch (IOException e) {
					this.logs.write("error trying to send sig");
				}
			} else if (this.task == RopeLocalRMI.STABALIZE_OTHERFT) {

				/*
				 * timing and logging
				 */
				this.logs.write("starting support stab of: " + this.myContact + " at "
						+ this.suppFingers.getTableOwner());
				long startTime = System.currentTimeMillis();

				int skipableFingers = this.pingShadows(this.editFingers, this.suppFingers.getTableOwner());

				/*
				 * Refresh each finger slot
				 */
				this.refreshFingers(this.editFingers, this.suppFingers, skipableFingers);

				/*
				 * Refresh our predecessor and successor list, collectively our
				 * shadows
				 */
				this.refreshMyShadows();
				this.refreshMyFingerShadows();

				/*
				 * Reporting timing
				 */
				this.logs.write("building another node's table to sign took: "
						+ (System.currentTimeMillis() - startTime));
				
				if(DEBUG){
				    this.logs.write("built for sig: " + this.suppFingers.getTableOwner() + "\n"
						+ this.editFingers.toString() + "\nfor: " + this.myContact);
				}

				/*
				 * Send the signature back
				 */
				RopeRMIPacket outPacket = RopeRMIPacket.buildOutgoingPacket(this.suppFingers.getTableOwner(),
						RopeRMIPacket.SIG_RES);
				byte[] hash = this.editFingers.getHash();
				byte[] sig = this.myCryptoCache.sign(hash);
				outPacket.setSigRes(hash, sig);
				try {
					Socket tSocket = new Socket(this.myContact.getTheIP(), this.myContact.getPort());
					OutputStream tOut = tSocket.getOutputStream();
					tOut.write(outPacket.toBytes());
					tSocket.close();
				} catch (IOException e) {
					this.logs.write("error trying to send sig");
				}

				/*
				 * End timing info
				 */
				startTime = System.currentTimeMillis() - startTime;
				this.logs.write("a shadow's table build and signed in: " + startTime);
			} else if (this.task == RopeLocalRMI.ANNOUNCE) {
				/*
				 * Advertise ourself to other nodes
				 */
				//				HashSet<RopeContact> alreadyTold = new HashSet<RopeContact>();
				//				List<RopeID> fingerStarts = this.myContact.getTheID().getReverseFingers();
				//				RopeRMIRequest request = null;
				//				for (RopeID tempID : fingerStarts) {
				//					request = new RopeRMIRequest(this.editFingers.getPredacessor(tempID), this.myFTCache,
				//							this.myCryptoCache, this.myContact, RopeRMIRequest.PRED, tempID);
				//					if (request.runRequest()) {
				//						if (!alreadyTold.contains(request.getResult())) {
				//							this.sendResponselessPacket(request.getResult(), RopeRMIPacket.ANNOUNCE);
				//							alreadyTold.add(request.getResult());
				//						}
				//					}
				//				}
				//				if (!alreadyTold.contains(this.editFingers.getMySuccSlot(0))) {
				//					this.sendResponselessPacket(this.editFingers.getMySuccSlot(0), RopeRMIPacket.ANNOUNCE);
				//				}
				this.sendResponselessPacket(this.editFingers.getMyPredSlot(0), RopeRMIPacket.ANNOUNCE);
				this.sendResponselessPacket(this.editFingers.getMySuccSlot(0), RopeRMIPacket.ANNOUNCE);
			} else {
				this.logs.write("not implemented yet (other local RMI tasks)!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-3);
		}
	}

	private void refreshMyShadows() {
		RopeContact[][] newShadows = this.runShadowRefresh(this.myContact.getTheID());
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			if (newShadows[0][counter] != null) {
				this.editFingers.offerFinger(newShadows[0][counter]);
			}
			if (newShadows[1][counter] != null) {
				this.editFingers.offerFinger(newShadows[1][counter]);
			}
		}
	}

	//XXX need to make sure and add ourself if needed since we're not yet in FTs, at least till a stabalize
	private void refreshMyFingerShadows() {
		RopeContact[][] returnedShadows = null;
		HashSet<RopeID> uniqueFingers = this.editFingers.getShadowedFingers();

		for (RopeID tempID : uniqueFingers) {
			returnedShadows = this.runShadowRefresh(tempID);
			if (this.suppFingers != null) {
				//this.logs.write("for: " + tempID + " for who's table: " + this.editFingers.getTableOwner() + " I am: " + this.suppFingers.getTableOwner() + " result: " + Arrays.toString(returnedShadows[1]) + " : " + Arrays.toString(returnedShadows[0]));
			}
			this.editFingers.setFingerShadows(tempID, returnedShadows[1], returnedShadows[0]);
		}
	}

	//TODO pick more efficient starting point for lookup, esp pred...
	private RopeContact[][] runShadowRefresh(RopeID startingID) {
		RopeContact shadows[][] = new RopeContact[2][FingerTable.SUCC_COUNT];
		RopeID currSucc = startingID;
		RopeRMIRequest request;
		boolean lookupError = false;
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			currSucc = currSucc.incrimentID();
			if (this.suppFingers != null) {
				//this.logs.write("more spam from " + this.suppFingers.getTableOwner() + " lookup: " + currSucc + " start: " + this.suppFingers.getSuccessor(currSucc));
				request = new RopeRMIRequest(this.suppFingers.getSuccessor(currSucc), this.myFTCache,
						this.myCryptoCache, this.myFTCache.getMe(), RopeRMIRequest.SUCC, currSucc, this.logs);
			} else {
				request = new RopeRMIRequest(this.editFingers.getMySuccSlot(0), this.myFTCache, this.myCryptoCache,
						this.myFTCache.getMe(), RopeRMIRequest.SUCC, currSucc, this.logs);
			}
			if (request.runRequest()) {
				RopeContact resp = request.getResult();
				if (resp.getTheID().equals(startingID)) {
					break;
				}

				shadows[0][counter] = resp;
				if (this.suppFingers != null) {
					//this.logs.write("spam from " + this.suppFingers.getTableOwner() + " lookup: " + currSucc + " result: " + shadows[0][counter]);
				}
				currSucc = resp.getTheID();
			} else {
				//this.logs.write("incomplete successor rebuild");
				lookupError = true;
				break;
			}
		}

		RopeID currPred = startingID;
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			if (this.suppFingers != null) {
				request = new RopeRMIRequest(this.suppFingers.getPredacessor(currPred), this.myFTCache,
						this.myCryptoCache, this.myFTCache.getMe(), RopeRMIRequest.PRED, currPred, this.logs);
			} else {
				request = new RopeRMIRequest(this.editFingers.getMySuccSlot(0), this.myFTCache, this.myCryptoCache,
						this.myFTCache.getMe(), RopeRMIRequest.PRED, currPred, this.logs);
			}
			if (request.runRequest()) {
				RopeContact resp = request.getResult();
				if (resp.getTheID().equals(startingID)) {
					break;
				}

				shadows[1][counter] = resp;
				currPred = resp.getTheID();
			} else {
				this.logs.write("incomplete predacessor rebuild");
				lookupError = true;
				break;
			}
		}

		/*
		 * If there is an error, don't mess with trying to predict if we're a
		 * shadow
		 */
		if (lookupError) {
			return shadows;
		}

		if (this.myContact.getTheID().equals(startingID)) {
			return shadows;
		}

		/*
		 * On bootstrap we need to check and see if we would be a better shadow
		 * for someone, and should add ourself
		 */
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			RopeID prev = null;
			RopeID next = null;

			if (shadows[0][counter] == null) {
				shadows[0][counter] = this.myContact;
				break;
			}

			if (counter == 0) {
				prev = startingID;
			} else {
				prev = shadows[0][counter - 1].getTheID();
			}
			next = shadows[0][counter].getTheID();

			if (this.myContact.getTheID().betweenExclusive(next, prev)) {
				for (int moveCounter = FingerTable.SUCC_COUNT - 2; moveCounter >= counter; moveCounter--) {
					shadows[0][moveCounter + 1] = shadows[0][moveCounter];
				}
				shadows[0][counter] = this.myContact;
				break;
			}
		}
		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			RopeID prev = null;
			RopeID next = null;

			if (shadows[1][counter] == null) {
				shadows[1][counter] = this.myContact;
				break;
			}

			if (counter == 0) {
				prev = startingID;
			} else {
				prev = shadows[1][counter - 1].getTheID();
			}
			next = shadows[1][counter].getTheID();

			if (this.myContact.getTheID().betweenExclusive(prev, next)) {
				for (int moveCounter = FingerTable.SUCC_COUNT - 2; moveCounter >= counter; moveCounter--) {
					shadows[1][moveCounter + 1] = shadows[0][moveCounter];
				}
				shadows[1][counter] = this.myContact;
				break;
			}
		}

		return shadows;
	}

	private boolean sendResponselessPacket(RopeContact dest, byte type) {
		try {
			Socket tempSocket = new Socket(dest.getTheIP(), dest.getPort());
			RopeRMIPacket tempPacket = RopeRMIPacket.buildOutgoingPacket(this.myContact, type);
			byte[] packetBytes = tempPacket.toBytes();

			OutputStream out = tempSocket.getOutputStream();
			out.write(packetBytes);
			tempSocket.close();
		} catch (IOException e) {
			this.logs.write("error tyring to send packet to: " + dest);
			return false;
		}
		return true;
	}

	private void askForFullCheckSigs(boolean stabalize) {
		/*
		 * Get our FT signed
		 */
		HashSet<RopeContact> myShadows = this.editFingers.getMyShadows();
		RopeRMIPacket sigreqPacket = null;

		if (stabalize) {
			sigreqPacket = RopeRMIPacket.buildOutgoingPacket(this.myContact, RopeRMIPacket.SIG_STAB_REQ);
		} else {
			sigreqPacket = RopeRMIPacket.buildOutgoingPacket(this.myContact, RopeRMIPacket.SIG_FULL_REQ);
		}

		if (stabalize) {
			sigreqPacket.setStabSigReqFT(this.editFingers);
		} else {
			sigreqPacket.setFullSigReqTS(this.editFingers.getLastUpdated());
		}

		byte[] sigreqBytes = sigreqPacket.toBytes();

		//this.logs.write("asking for sigs on: \n" + this.editFingers.toString() + "\nat: " + this.myContact);

		for (RopeContact tContact : myShadows) {
			try {
				Socket tSocket = new Socket(tContact.getTheIP(), tContact.getPort());
				OutputStream tOut = tSocket.getOutputStream();
				tOut.write(sigreqBytes);
				tSocket.close();
			} catch (IOException e) {
				this.logs.write("error asking for sig from: " + tContact);
			}
		}
	}

	private int pingShadows(FingerTable tableInQuestion, RopeContact replyContact) {

		Semaphore wall = new Semaphore(0);
		List<Pinger> pingers = new LinkedList<Pinger>();
		boolean succChanged = false;
		int skipableFingers = 0;

		for (int counter = 0; counter < FingerTable.SUCC_COUNT; counter++) {
			if (tableInQuestion.getMySuccSlot(counter) != null) {
				pingers.add(new Pinger(replyContact, tableInQuestion.getMySuccSlot(counter), wall, counter, true, this.logs));
			}
			if (tableInQuestion.getMyPredSlot(counter) != null) {
				pingers.add(new Pinger(replyContact, tableInQuestion.getMyPredSlot(counter), wall, counter, false, this.logs));
			}
		}

		for (Pinger tPinger : pingers) {
			Thread tThread = new Thread(tPinger);
			tThread.start();
		}

		for (int counter = 0; counter < pingers.size(); counter++) {
			try {
				wall.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		HashSet<Integer> predRemSlots = new HashSet<Integer>();
		HashSet<Integer> sucRemSlots = new HashSet<Integer>();
		for (Pinger tPinger : pingers) {
			if (!tPinger.isAlive()) {
				if (tPinger.isSuccessor()) {
					int slotReduceCount = 0;
					for (int tPos : sucRemSlots) {
						if (tPos < tPinger.getPosition()) {
							slotReduceCount++;
						}
					}
					tableInQuestion.successorDeath((tPinger.getPosition() - slotReduceCount));
					sucRemSlots.add(tPinger.getPosition());

					if (tPinger.getPosition() == 0) {
						succChanged = true;
					}
				} else {
					int slotReduceCount = 0;
					for (int tPos : predRemSlots) {
						if (tPos < tPinger.getPosition()) {
							slotReduceCount++;
						}
					}
					tableInQuestion.predDeath((tPinger.getPosition() - slotReduceCount));
					predRemSlots.add(tPinger.getPosition());
				}
			}
		}

		/*
		 * Update our successor, all depends on us having one, so do this first
		 */
		if (tableInQuestion.getMySuccSlot(0) == null) {
			//TODO what should we actually do here?
			this.logs.write("ALL NODES ARE DEAD IN FRONT OF ME: " + tableInQuestion.getTableOwner());
		}
		if (succChanged) {
			skipableFingers = this.editFingers.pushNewSuccIn();
		}

		return skipableFingers;
	}

	private void refreshFingers(FingerTable tableToChange, FingerTable tableToDoLookups, int startingPos) {

		Semaphore wall = new Semaphore(0);
		List<RopeRMIRequest> lookups = new LinkedList<RopeRMIRequest>();

		/*
		 * Refresh each finger slot
		 */
		List<RopeID> fingerStarts = tableToChange.getTableOwner().getTheID().getFingerStarts();
		for (int slot = fingerStarts.size() - 1; slot >= startingPos; slot--) {
			RopeID tID = fingerStarts.get(slot);
			RopeContact start = tableToDoLookups.getSuccessor(tID);
			RopeRMIRequest request = new RopeRMIRequest(start, this.myFTCache, this.myCryptoCache, this.myFTCache
					.getMe(), RopeRMIRequest.SUCC, tID, wall, slot, this.logs);
			lookups.add(request);
		}

		for (RopeRMIRequest tReq : lookups) {
			Thread tThread = new Thread(tReq);
			tThread.start();
		}

		//this.logs.write("before wall at: " + tableToDoLookups.getTableOwner() + " for: "
		//		+ tableToChange.getTableOwner());

		for (int counter = 0; counter < lookups.size(); counter++) {
			try {
				wall.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		//this.logs.write("after wall at: " + tableToDoLookups.getTableOwner() + " for: "
		//		+ tableToChange.getTableOwner());

		for (RopeRMIRequest tReq : lookups) {
			if (tReq.isRequestSuccess()) {
				tableToChange.overrideFinger(tReq.getSlot(), tReq.getResult());
			}
		}
	}

	private void refreshShadowSet(FingerTable editTable, FingerTable baseTable, RopeContact replyNode,
			HashSet<RopeID> shadowedSet) {
		Semaphore wall = new Semaphore(0);
		List<ShadowRefresher> refreshers = new LinkedList<ShadowRefresher>();

		for (RopeID tContact : shadowedSet) {
			refreshers.add(new ShadowRefresher(tContact, this.myFTCache.getMe(), editTable.getTableOwner(), baseTable,
					this.myFTCache, this.myCryptoCache, true, wall, this.logs));
			refreshers.add(new ShadowRefresher(tContact, this.myFTCache.getMe(), editTable.getTableOwner(), baseTable,
					this.myFTCache, this.myCryptoCache, false, wall, this.logs));
		}

		for (ShadowRefresher tReff : refreshers) {
			Thread tThread = new Thread(tReff);
			tThread.start();
		}

		//this.logs.write("before shadow wall: " + this.myFTCache.getMe() + " need " + refreshers.size());
		for (int counter = 0; counter < refreshers.size(); counter++) {
			try {
				wall.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//this.logs.write("after shadow wall: " + this.myFTCache.getMe());

		HashSet<RopeID> cleared = new HashSet<RopeID>();
		for (ShadowRefresher tReff : refreshers) {
			if (editTable.getTableOwner().getTheID().equals(tReff.getBaseNode())) {
				RopeContact[] result = tReff.getResult();
				for (int counter = 0; counter < result.length; counter++) {
					if (result[counter] != null) {
						//this.logs.write("I see: " + result[counter]);
						editTable.offerFinger(result[counter]);
					}
				}
			} else {
				if (!cleared.contains(tReff.getBaseNode())) {
					editTable.resetFingerShadows(tReff.getBaseNode());
					cleared.add(tReff.getBaseNode());
				}
				editTable.setFingerShadows(tReff.getBaseNode(), tReff.getResult());
			}
		}
	}
}
