package rope.pub.data;

import java.net.InetAddress;
import java.net.UnknownHostException;

import rope.io.ByteOps;

public class RopeContact {

	private RopeID theID;

	private InetAddress theIP;

	private int port;

	public static final int BYTE_SIZE = RopeID.ID_BYTE_LEN + 8;

	public RopeContact(RopeID theID, InetAddress theIP, int port) {
		super();
		this.theID = theID;
		this.theIP = theIP;
		this.port = port;
	}

	public static RopeContact parseBytesToContact(byte[] wireBytes) throws UnknownHostException {
		
		boolean notNull = false;
		for(int counter = 0; counter < RopeContact.BYTE_SIZE; counter++){
			if(wireBytes[counter] != 0x00){
				notNull = true;
				break;
			}
		}
		
		if(!notNull){
			return null;
		}
		
		byte[] idBytes = ByteOps.subArray(wireBytes, 0, RopeID.ID_BYTE_LEN);
		byte[] ipBytes = ByteOps.subArray(wireBytes, RopeID.ID_BYTE_LEN, 4);
		byte[] portBytes = ByteOps.subArray(wireBytes, RopeID.ID_BYTE_LEN + 4, 4);
		
		return new RopeContact(new RopeID(idBytes), InetAddress.getByAddress(ipBytes), ByteOps.wordToInt(portBytes));
	}

	public static byte[] getBytes(RopeContact node) {
		if(node == null){
			byte[] retBytes = new byte[RopeContact.BYTE_SIZE];
			for(int counter = 0; counter < RopeContact.BYTE_SIZE; counter++){
				retBytes[counter] = 0x00;
			}
			return retBytes;
		}
		
		byte[] retBytes = node.getTheID().getBytes();
		retBytes = ByteOps.catByteArrays(retBytes, node.getTheIP().getAddress());
		retBytes = ByteOps.catByteArrays(retBytes, ByteOps.intToWord(node.getPort()));
		return retBytes;
	}

	public RopeID getTheID() {
		return theID;
	}

	public InetAddress getTheIP() {
		return theIP;
	}

	public int getPort() {
		return port;
	}

	public String toString() {
		return theID.toString();
		//return "RopeContact [port=" + port + ", theID=" + theID + ", theIP=" + theIP + "]";
	}

	public int hashCode() {
		return this.toString().hashCode();
	}

	public boolean equals(Object rhs) {
		RopeContact rhsContact = (RopeContact) rhs;
		return this.theID.equals(rhsContact.theID) && this.theIP.equals(rhsContact.theIP)
				&& this.port == rhsContact.port;
	}
}
