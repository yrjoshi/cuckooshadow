package rope.pub.data;

import java.math.BigInteger;
import java.util.*;

/**
 * 
 * @author pendgaft
 * 
 */
public class RopeID implements Comparable<RopeID> {

	/**
	 * The object that stores the actual value, used over a byte array to make
	 * the math a bit easier.
	 */
	private BigInteger idValue;

	/**
	 * Defines the size of IDs used in bytes. IDs that don't align on byte sizes
	 * are not allowed.
	 */
	public static final int ID_BYTE_LEN = 20;

	/**
	 * The upper bound on how large a Chord key can be. The ID must be less then
	 * this value.
	 */
	private static final BigInteger MODULUS = BigInteger.ONE.shiftLeft(RopeID.ID_BYTE_LEN * 8);
	
	//XXX real impl this should be secure random
	private static Random rng = new Random(1);

	/**
	 * Creates a new random RopeID of the correct length.
	 */
	public RopeID() {
		this.idValue = new BigInteger(RopeID.ID_BYTE_LEN * 8, RopeID.rng);
	}

	/**
	 * Creates a new RopeID object for the given ID in bytes.
	 * 
	 * @param bytes
	 *            - the bytes of the ID
	 */
	public RopeID(byte[] bytes) {
		if (bytes.length != RopeID.ID_BYTE_LEN) {
			throw new IllegalArgumentException("Bad byte length: " + bytes.length + " should be: " + RopeID.ID_BYTE_LEN);
		}

		this.idValue = new BigInteger(1, bytes);
	}

	/**
	 * Internal constructor that allows for the direct creation of RopeIDs. This
	 * should not be exposed to the world.
	 * 
	 * @param intValue
	 *            - the ID value this node should have
	 */
	private RopeID(BigInteger intValue) {
		this.idValue = intValue;
	}

	/**
	 * Generates a byte array that represents this RopeID.
	 * 
	 * @return - a byte array of ID_BYTE_LEN size storing the byte value of the
	 *         ID
	 */
	public byte[] getBytes() {
		byte[] exportValue = new byte[RopeID.ID_BYTE_LEN];
		byte[] myBytes = this.idValue.toByteArray();

		int neededBytes = (int) Math.ceil(this.idValue.bitLength() / 8.0);
		int exportOffset = exportValue.length - neededBytes;
		int myOffset = myBytes.length - neededBytes;

		for (int counter = 0; counter < neededBytes; counter++) {
			exportValue[exportOffset + counter] = myBytes[myOffset + counter];
		}

		return exportValue;
	}

	/**
	 * Returns a RopeID that is 2^ (step - 1) away from the ID. Used to generate
	 * fingers, etc.
	 * 
	 * @param step
	 *            - the bit position we want to shift from
	 * @param reverse
	 *            - true if we are moving in the negetive direction, false for
	 *            the positive
	 * @return - the id 2 ^ (step -1) forward or reverse depending on value of
	 *         reverse
	 */
	private RopeID getNextID(int step, boolean reverse) {
		if (reverse) {
			return new RopeID(this.idValue.subtract(BigInteger.ONE.shiftLeft(step)).mod(RopeID.MODULUS));
		}
		return new RopeID(this.idValue.add(BigInteger.ONE.shiftLeft(step)).mod(RopeID.MODULUS));
	}

	/**
	 * Currently inclusive to front, exclusive to back
	 * 
	 * @param front
	 * @param back
	 * @return
	 */
	public boolean betweenExclusive(RopeID front, RopeID back) {
		
		BigInteger tempFront = front.idValue;
		BigInteger tempBack = back.idValue;
		BigInteger tempMid = this.idValue;
		if(tempFront.compareTo(tempBack) < 0){
			tempFront = tempFront.add(RopeID.MODULUS);
		}
		if(tempMid.compareTo(tempBack) < 0){
			tempMid = tempMid.add(RopeID.MODULUS);
		}
		
		return (tempFront.compareTo(tempMid) > 0) && (tempMid.compareTo(tempBack) > 0);
	}
	
	public boolean betweenInclusive(RopeID front, RopeID back) {
		if(back.idValue.equals(this.idValue) || front.idValue.equals(this.idValue)){
			return true;
		}
		return this.betweenExclusive(front, back);
	}
	
	public boolean betweenInclusiveBackward(RopeID front, RopeID back){
		if(this.idValue.equals(back.idValue)){
			return true;
		}
		
		return this.betweenExclusive(front, back);
	}
	
	public boolean betweenInclusiveForward(RopeID front, RopeID back){
		if(this.idValue.equals(front.idValue)){
			return true;
		}
		return this.betweenExclusive(front, back);
	}

	/**
	 * Gets a list of the starts of finger table ranges. This is defined in the
	 * Chord paper as finger.start
	 * 
	 * @return - a list of RopeID objects representing the finger table starts
	 */
	public List<RopeID> getFingerStarts() {
		List<RopeID> retList = new ArrayList<RopeID>(RopeID.ID_BYTE_LEN * 8);

		for (int counter = 0; counter < RopeID.ID_BYTE_LEN * 8; counter++) {
			retList.add(this.getNextID(counter, false));
		}

		return retList;
	}

	public List<RopeID> getReverseFingers() {
		List<RopeID> retList = new ArrayList<RopeID>(RopeID.ID_BYTE_LEN * 8);
		
		for(int counter = 0; counter < RopeID.ID_BYTE_LEN * 8; counter++){
			retList.add(this.getNextID(counter, true));
		}
		
		return retList;
	}
	
	public RopeID incrimentID(){
		return this.getNextID(1, false);
	}

	public int compareTo(RopeID rhs) {
		return this.idValue.compareTo(rhs.idValue);
	}

	public boolean equals(Object rhs) {
		RopeID rhsID = (RopeID) rhs;

		return this.idValue.equals(rhsID.idValue);
	}

	public String toString() {
		return this.idValue.toString(16);
	}

	public int hashCode() {
		return this.idValue.hashCode();
	}
}
