package rope.pub.core;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.util.concurrent.Semaphore;

import rope.pub.data.*;
import rope.priv.data.*;
import rope.priv.core.MaxLogger;
import rope.priv.core.RopeLocalRMI;
import rope.priv.core.SuicideBomb;
import rope.io.*;

//EXIT CODES
// -1 = config error
// -2 = network error

public class RopeNode implements Runnable {

	private RopeContact myContact = null;

	private FingerTable myFingerTable = null;

	private FingerTable fetusFingerTable = null;

	private FingerTableCache myFTCache = null;

	private RopeListener myListener = null;

	private CryptoCache myCryptoCache = null;

	private boolean running = false;

	private Semaphore fetusFlag = null;

	private Semaphore mainTableFlag = null;
	
	private int skipCount = 0;

	private SuicideBomb theBomb = null;
	
	private MaxLogger logs = null;
	
	public static final long STAB_INTERVAL = 60000;
	
	public static final int SKIP_MAX = 1;

	public static final long TTL = 30000;
	
	public static void main(String args[]) throws InterruptedException, IOException {

		/*
		 * Launches the two well known nodes, this should only be launched on
		 * waterhouse.cs
		 */
		/*
		 * TODO when booting well known nodes in the future, boot a number equal
		 * to the number of shadows, this way we avoid anything "odd" because of
		 * not having enough shadows
		 */
		if (args.length == 0) {
			if (!InetAddress.getLocalHost().equals(InetAddress.getByName("146.57.249.104"))) {
				System.err.println("WATERHOUSE HAS CHANGED IPs!!!!!");
				System.exit(-2);
			}

			byte[] idA = new byte[RopeID.ID_BYTE_LEN];
			byte[] idB = new byte[RopeID.ID_BYTE_LEN];
			idA[0] = (byte) 0;
			idB[0] = (byte) 128;

			RopeContact contactA = new RopeContact(new RopeID(idA), InetAddress.getByName("146.57.249.104"), 14000);
			RopeContact contactB = new RopeContact(new RopeID(idB), InetAddress.getByName("146.57.249.104"), 14001);

			RopeNode nodeA = new RopeNode(contactA, "wkA.log");
			RopeNode nodeB = new RopeNode(contactB, "wkB.log");

			RopeContact[][] aShadows = new RopeContact[2][FingerTable.SUCC_COUNT];
			RopeContact[][] bShadows = new RopeContact[2][FingerTable.SUCC_COUNT];

			aShadows[0][0] = contactB;
			aShadows[1][0] = contactB;

			bShadows[0][0] = contactA;
			bShadows[1][0] = contactA;

			nodeA.hack(contactB, bShadows, nodeB.myCryptoCache);
			nodeB.hack(contactA, aShadows, nodeA.myCryptoCache);
		}
		/*
		 * This branch launches a normal node, the args should be <id> <port>
		 * <uptime (ms>
		 */
		else {
			/*
			 * Build node, join the network
			 */
			SecureRandom rng = new SecureRandom();
			byte id[] = new byte[RopeID.ID_BYTE_LEN];
			rng.nextBytes(id);
			int port = Integer.parseInt(args[0]);
			RopeContact me = new RopeContact(new RopeID(id), InetAddress.getLocalHost(), port);
			System.out.println("MY ID: " + me);
			RopeNode myNode = new RopeNode(me, "logfile.log");
			myNode.join();

			/*
			 * Live until we are suppose to churn out, then churn out
			 */
			long upTime = Long.parseLong(args[1]);
			Thread.sleep(upTime);
			myNode.leave();
		}

	}

	public RopeNode(RopeContact givenContact, String logfileName) throws IOException {
		this.logs = new MaxLogger(logfileName);
		
		this.myContact = givenContact;
		this.myFingerTable = null;
		this.myFTCache = new FingerTableCache(this.myContact, this.logs);
		this.myCryptoCache = new CryptoCache(this.myContact);
		this.fetusFlag = new Semaphore(0);
		this.mainTableFlag = new Semaphore(1);

		try {
			this.myListener = new RopeListener(this.myContact, this.myFingerTable, this.myFTCache, this.myCryptoCache,
					this, this.logs);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-2);
		}
		this.myListener.bootUp();
	}

	public RopeNode(int port, String logfileName) throws IOException {
		this.logs = new MaxLogger(logfileName);
		
		try {
			this.myContact = new RopeContact(new RopeID(), InetAddress.getLocalHost(), port);
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
			System.exit(-2);
		}
		this.myFingerTable = null;
		this.myFTCache = new FingerTableCache(this.myContact, this.logs);
		this.myCryptoCache = new CryptoCache(this.myContact);
		this.fetusFlag = new Semaphore(0);
		this.mainTableFlag = new Semaphore(1);

		try {
			this.myListener = new RopeListener(this.myContact, this.myFingerTable, this.myFTCache, this.myCryptoCache,
					this, this.logs);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-2);
		}

		this.myListener.bootUp();
	}

	/**
	 * Runnable routine does stabalize
	 */
	public void run() {
		while (this.running) {
			try {
				Thread.sleep(RopeNode.STAB_INTERVAL);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return;
			}

			if (this.running) {
				this.stabalize();
			}
		}

		//System.out.println(this.toString());
	}

	private void stabalize() {
		if (this.fetusFingerTable != null) {
			this.skipCount++;
			
			if (this.skipCount > RopeNode.SKIP_MAX) {
				try {
					this.mainTableFlag.acquire();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
							
				
				this.myFingerTable = this.fetusFingerTable;
				this.myListener.setFingerTable(this.myFingerTable);
				this.myFTCache.updateMyFT(this.myFingerTable);
				this.fetusFingerTable = null;
				
				this.skipCount = 0;
				this.mainTableFlag.release();
				this.fetusFlag.release();
				this.logs.write("PROMOTING TABLE ANYWAY AT: " + this.myContact);
				
			} else {
				this.logs.write("SKIPPING STABALIZE at node: " + this.myContact + " count: " + this.skipCount);
				return;
			}
		}

		this.logs.write("stabalizing: " + this.myContact + " started at: " + System.currentTimeMillis());
		this.createFetus();
		RopeLocalRMI temp = new RopeLocalRMI(this.myContact, this.fetusFingerTable, this.myFTCache, this.myCryptoCache,
				RopeLocalRMI.STABALIZE, null, this.logs);
		Thread tempThread = new Thread(temp);
		tempThread.start();
	}

	public void createFetus() {
		try {
			this.fetusFlag.acquire();
			this.fetusFingerTable = new FingerTable(this.myFingerTable.dumpTableToBytes());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public FingerTable getFetus() {
		return this.fetusFingerTable;
	}

	public void join() {
		this.logs.write("bootstarp started at: " + System.currentTimeMillis());
		this.running = true;
		this.theBomb = new SuicideBomb(RopeNode.TTL, this.logs);
		Thread bombThread = new Thread(this.theBomb);
		bombThread.start();
		this.fetusFingerTable = new FingerTable(this.myContact, System.currentTimeMillis());
		RopeLocalRMI temp = new RopeLocalRMI(this.myContact, this.fetusFingerTable, this.myFTCache, this.myCryptoCache,
				RopeLocalRMI.BOOTSTRAP, null, this.logs);
		Thread tempThread = new Thread(temp);
		tempThread.start();

		Thread ftCacheCleaner = new Thread(this.myFTCache);
		ftCacheCleaner.setDaemon(true);
		ftCacheCleaner.start();
	}

	public void findSuccessor(RopeID targetID) {

	}

	public void leave() {
		this.running = false;
		this.myListener.shutdown();
	}

	public void kill() {
		this.leave();
		this.myListener.shutdown();
		this.logs.doneLogging();
	}

	public void postSig(byte[] tableHash, RopeContact signer, byte[] sig, boolean suppressAnnounce) {

		try {
			this.mainTableFlag.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (this.myFingerTable == null) {
			//TODO error message here plox?
			if (this.fetusFingerTable != null) {
				/*
				 * Make sure the sig is valid
				 */
				byte[] currHash = this.fetusFingerTable.getHash();
				for (int counter = 0; counter < currHash.length; counter++) {
					if (currHash[counter] != tableHash[counter]) {
						System.err.println("bad opening ft");
						this.mainTableFlag.release();
						return;
					}
				}

				this.fetusFingerTable.addSig(signer, sig);
				if (this.fetusFingerTable.getSigs().size() >= FingerTable.VALID_THRESH) {
					if (this.theBomb != null) {
						this.theBomb.disarm();
					}
					
					this.logs.write("bootstrap now valid " + this.myContact + " at " + System.currentTimeMillis());

					this.myFingerTable = this.fetusFingerTable;
					this.myListener.setFingerTable(this.myFingerTable);
					this.myFTCache.updateMyFT(this.myFingerTable);
					this.fetusFingerTable = null;
					
					if (!suppressAnnounce) {
						Thread myThread = new Thread(this);
						myThread.start();
						
						RopeLocalRMI announceRMI = new RopeLocalRMI(this.myContact, this.myFingerTable,
								this.myFTCache, this.myCryptoCache, RopeLocalRMI.ANNOUNCE, null, this.logs);
						Thread announceThread = new Thread(announceRMI);
						announceThread.start();
					}

					this.fetusFlag.release();
					this.mainTableFlag.release();
					return; 
				}
			}
		}

		boolean foundHome = true;
		if (this.fetusFingerTable != null) {
			/*
			 * Make sure the sig is valid
			 */
			byte[] currHash = this.fetusFingerTable.getHash();
			for (int counter = 0; counter < currHash.length; counter++) {
				if (currHash[counter] != tableHash[counter]) {
					foundHome = false;
					break;
				}
			}

			if (foundHome) {
				this.fetusFingerTable.addSig(signer, sig);
				this.logs.write("valid sig for fetus " + this.myContact);
				if (this.fetusFingerTable.getSigs().size() >= FingerTable.VALID_THRESH) {
					this.logs.write("stabalize now valid " + this.myContact + " at " + System.currentTimeMillis());

					this.myFingerTable = this.fetusFingerTable;
					this.myListener.setFingerTable(this.myFingerTable);
					this.myFTCache.updateMyFT(this.myFingerTable);
					this.fetusFingerTable = null;
					this.fetusFlag.release();
					this.mainTableFlag.release();
					return;
				}
			}
		}

		foundHome = true;
		byte[] currHash = this.myFingerTable.getHash();
		for (int counter = 0; counter < currHash.length; counter++) {
			if (currHash[counter] != tableHash[counter]) {
				foundHome = false;
				break;
			}
		}

		if (foundHome) {
			this.myFingerTable.addSig(signer, sig);
			this.logs.write("valid sig for current FT " + this.myContact);
			this.mainTableFlag.release();
			return;
		}

		this.logs.write("INVALID sig recieved at: " + this.myContact);
		this.mainTableFlag.release();
	}

	public String toString() {
		return "RopeNode [myContact=" + myContact + "]\n[myFingerTable=" + myFingerTable + "]";
	}

	public void hack(RopeContact hacked, RopeContact[][] hackedFingers, CryptoCache otherCrypto) {
		if (!this.running) {
			this.running = true;
			Thread myThread = new Thread(this);
			myThread.start();
		}

		if (this.myFingerTable == null && this.fetusFingerTable == null) {
			this.fetusFingerTable = new FingerTable(this.myContact, System.currentTimeMillis());
		}

		if (this.myFingerTable == null) {
			if (this.fetusFingerTable.offerFinger(hacked)) {
				this.fetusFingerTable.setFingerShadows(hacked.getTheID(), hackedFingers[1], hackedFingers[0]);
			}
		} else {
			if (this.myFingerTable.offerFinger(hacked)) {
				this.myFingerTable.setFingerShadows(hacked.getTheID(), hackedFingers[1], hackedFingers[0]);
			}
		}

		byte[] hash = null;
		if (this.myFingerTable == null) {
			hash = this.fetusFingerTable.getHash();
		} else {
			hash = this.myFingerTable.getHash();
		}
		byte[] sig = otherCrypto.sign(hash);
		this.postSig(hash, hacked, sig, true);
	}
}
