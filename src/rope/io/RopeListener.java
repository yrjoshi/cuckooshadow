package rope.io;

import java.io.*;
import java.net.*;

import rope.priv.core.MaxLogger;
import rope.priv.core.RopeRemoteRMI;
import rope.priv.data.*;
import rope.pub.data.*;
import rope.pub.core.*;

/**
 * Simple daemon that handles the accepting of incoming connections to a Rope
 * node. Should be ran in a thread.
 * 
 */
public class RopeListener implements Runnable {

	/**
	 * The socket we listen for connections on
	 */
	private ServerSocket myIncomingSocket = null;

	/**
	 * My contact information, needed to send responses
	 */
	private RopeContact myContact = null;

	/**
	 * My finger table, needed to build responses to querries.
	 */
	private FingerTable myFingerTable = null;

	private FingerTableCache myCache = null;

	private CryptoCache myCryptoCache = null;

	private RopeNode myNode = null;
	
	private MaxLogger logs = null;

	/**
	 * Flag used to tell the daemon to shut down
	 */
	private boolean running = false;

	/**
	 * Builds a listener, nothing really special. This does NOT connect the
	 * socket/start up the thread.
	 * 
	 * @param myContact
	 *            - the contact information for this node, needed to build
	 *            response packets
	 * @param myFT
	 *            - the finger table of this node, needed to generate response
	 *            to queries
	 * @throws IOException
	 *             - if there is an issue with creating the server socket
	 */
	public RopeListener(RopeContact myContact, FingerTable myFT, FingerTableCache myCache, CryptoCache myCryptoCache,
			RopeNode myNode, MaxLogger log) throws IOException {
		this.myContact = myContact;
		this.myFingerTable = myFT;
		this.myCache = myCache;
		this.myCryptoCache = myCryptoCache;
		this.myNode = myNode;
		this.logs = log;
		this.myIncomingSocket = new ServerSocket(this.myContact.getPort(), 50, this.myContact.getTheIP());
	}

	/**
	 * Turns the listener on, this will cause a daemon thread to be spawned,
	 * which will accept connections, spinning them off into threads of their
	 * own
	 */
	public void bootUp() {
		this.running = true;
		Thread tempThread = new Thread(this);
		tempThread.setDaemon(true);
		tempThread.start();
	}

	/**
	 * Simple accept connection, spawn child, do it again till told to stop,
	 * function
	 */
	public void run() {
		Socket incSocket = null;
		RopeRemoteRMI rmiHandler = null;
		Thread childThread = null;

		try {
			while (this.running) {
				incSocket = this.myIncomingSocket.accept();
				rmiHandler = new RopeRemoteRMI(incSocket, this.myContact, this.myFingerTable, this.myCache,
						this.myCryptoCache, this.myNode, this.logs);
				childThread = new Thread(rmiHandler);
				childThread.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Closes the listener down, this will typically result in an IOException
	 * being thrown in the run method, but it should be handled gracefully.
	 */
	public void shutdown() {
		this.running = false;
		try {
			this.myIncomingSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setFingerTable(FingerTable newFT) {
		this.myFingerTable = newFT;
	}
}
