package rope.io;

/**
 * ByteOps.java
 * Copyright 2008 Maxfield Schuchard
 *
 *     This file is part of Sanguinus.
 *
 *  Sanguinus is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Sanguinus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Sanguinus.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Collection of static methods used for byte operations by cryptographic
 * classes.
 * 
 */
public class ByteOps {

	public static final byte[] ZERO = { 0x0, 0x0, 0x0, 0x0 };

	/**
	 * Static method used to convert an int into a byte array of size 4 (32 bit
	 * word).
	 * 
	 * @param value
	 *            an integer that will be converted into a byte array.
	 * @return a big-endian byte array representation of value, the array will
	 *         be size 4 always.
	 */
	public static byte[] intToWord(int value) {
		return new byte[] { (byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value };
	}

	/**
	 * Static method used to convert a long into a byte array of size 8 (64 bit
	 * word).
	 * 
	 * @param value
	 *            a long that will be converted into a byte array.
	 * @return a big-endian byte array representation of value, the array will
	 *         be size 8 always.
	 */
	public static byte[] longToWord(long value) {
		byte[] buf = new byte[8];
		int tmp = (int) (value >>> 32);

		buf[0] = (byte) (tmp >>> 24);
		buf[1] = (byte) ((tmp >>> 16) & 0x0ff);
		buf[2] = (byte) ((tmp >>> 8) & 0x0ff);
		buf[3] = (byte) tmp;
		tmp = (int) value;
		buf[4] = (byte) (tmp >>> 24);
		buf[5] = (byte) ((tmp >>> 16) & 0x0ff);
		buf[6] = (byte) ((tmp >>> 8) & 0x0ff);
		buf[7] = (byte) tmp;

		return buf;
	}

	/**
	 * Static method used to convert a 32 bit word, in the form of a byte array
	 * of size 4, into an int.
	 * 
	 * @param value
	 *            a byte array of size 4 that will be converted into an int,
	 *            assumed to be big-endian.
	 * @return the integer representation of the big-endian byte array.
	 */
	public static int wordToInt(byte[] value) {
		if (value == null || value.length != 4) {
			throw new IllegalArgumentException("Invalid array, should be size 4.");
		}

		return (value[0] << 24) + ((value[1] & 0xFF) << 16) + ((value[2] & 0xFF) << 8) + (value[3] & 0xff);
	}

	/**
	 * Static method used to convert a 64 bit word, in the form of a byte array
	 * of size 8, into an double.
	 * 
	 * @param value
	 *            a byte array of size 8 that will be converted into an double,
	 *            assumed to be big-endian.
	 * @return the long representation of the big-endian byte array.
	 */
	public static long wordToLong(byte[] value) {
		if (value == null || value.length != 8) {
			throw new IllegalArgumentException("Invalid array, should be size 4.");
		}

		long retVal = ByteOps.wordToInt(ByteOps.subArray(value, 0, 4)) * (long) Math.pow(2, 32)
				+ ByteOps.wordToInt(ByteOps.subArray(value, 4, 4));
		return retVal;
	}

	/**
	 * Static method used to cat together two byte arrays of any size.
	 * 
	 * @param lhs
	 *            the byte array that will be added to the result first, must be
	 *            non-null.
	 * @param rhs
	 *            the byte array that will be added to the result second, must
	 *            be non-null.
	 * @return a byte array that is the concatenation of lhs and rhs.
	 */
	public static byte[] catByteArrays(byte[] lhs, byte[] rhs) {
		if (lhs == null || rhs == null) {
			throw new NullPointerException("One or more of the arrays passed was null.");
		}

		byte[] retValue = new byte[lhs.length + rhs.length];

		for (int counter = 0; counter < lhs.length; counter++) {
			retValue[counter] = lhs[counter];
		}
		for (int counter = 0; counter < rhs.length; counter++) {
			retValue[lhs.length + counter] = rhs[counter];
		}

		return retValue;
	}

	/**
	 * Static method used to generate a byte array that is a subsection of a
	 * larger array. This method creates a copy of the passed array, leaving the
	 * original unchanged.
	 * 
	 * @param array
	 *            the array we want to take our subsection from, must be
	 *            non-null.
	 * @param start
	 *            that offset we want to start from in array, acceptable range
	 *            is (0, array.length].
	 * @param length
	 *            the number of bytes from array we wish to include in our new
	 *            array, acceptable range is (1, array.length - start)
	 * @return a new byte array containing the byte values from array[start] to
	 *         array[start + length] in order.
	 */
	public static byte[] subArray(byte[] array, int start, int length) {
		//yay more param checking then actual lines of code....
		if (array == null) {
			throw new NullPointerException("Null array passed.");
		}
		if (start < 0 || start >= array.length) {
			throw new IllegalArgumentException("Start argument must be in range (0, array.length].");
		}
		if (length < 1 || length > array.length - start) {
			throw new IllegalArgumentException("Length argument must be in range (1, array.length - start).");
		}

		byte[] retArray = new byte[length];

		for (int counter = 0; counter < length; counter++) {
			retArray[counter] = array[start + counter];
		}

		return retArray;
	}

	/**
	 * Preforms a right rotate and carry operation on an int. This operation is
	 * simply a right shift, followed by placing the bits that fell off on the
	 * left hand side of the int.
	 * 
	 * @param value
	 *            the int to be right rotated.
	 * @param dist
	 *            the number of places to be rotated by (will be treated as mod
	 *            32).
	 * @return the int resulting from the right rotate.
	 */
	public static int rightRotate(int value, int dist) {

		//make it positive mod 32 for speed
		if (dist < 0) {
			dist += 32;
		}
		dist = dist % 32;

		//store the drop off, logical right shift and carry
		int carry = value << (32 - dist);
		value = value >>> dist;
		return carry | value;
	}

	/**
	 * Computes the XOR of two same size byte arrays.
	 * 
	 * @param lhs
	 *            - one array
	 * @param rhs
	 *            - the other array
	 * @throws IllegalArguementException
	 *             - if the byte arrays are not the same length
	 * @return - lhs XOR rhs
	 */
	public static byte[] xor(byte[] lhs, byte[] rhs) {
		if (lhs == null || rhs == null) {
			throw new NullPointerException("All arugments non-optional.");
		}
		if (lhs.length != rhs.length) {
			throw new IllegalArgumentException("The arrays must be the same size.");
		}

		byte[] retBytes = new byte[lhs.length];

		for (int counter = 0; counter < lhs.length; counter++) {
			retBytes[counter] = (byte) (lhs[counter] ^ rhs[counter]);
		}

		return retBytes;
	}

	/**
	 * Creates a hex string that can be printed showing the current byte array.
	 * 
	 * Might be an error with a leading "0" currently....
	 * 
	 * @param bytes
	 *            - the byte array we want to print
	 * @return - a hex string of the byte array
	 */
	public static String printableByteString(byte[] bytes) {
		String out = "";

		for (int counter = 0; counter < bytes.length; counter++) {
			String cat = "" + Integer.toHexString(bytes[counter] & 0xff);
			if (cat.length() == 1) {
				cat = "0" + cat;
			}
			out += cat;
		}

		return out;
	}
}
